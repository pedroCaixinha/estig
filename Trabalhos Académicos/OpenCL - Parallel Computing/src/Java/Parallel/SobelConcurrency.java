/*
Sobel Edge Detection Filter

Authors: Pedro Caixinha 4437 and Miguel Rosa 6219
Last Modification Date: 12/06/2016 

This program implements an Edge Detection Algorithm 
using the Sobel Operator.
The algorithm is applied to a 3620x3620 grayscale image.
*/

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.Color;
import java.lang.Math;

public class SobelConcurrency {

    final static int EXPS = 50;

    /*
     * Main method. Runs the program
     */
    public static void main(String[] args) {
	    BufferedImage image;
        try {
            image = ImageIO.read(new File("../../Images/Lenna.png"));
            int[][] pixelData = SobelConcurrency.bufToArray(image);

            int[][] xKernel = {{-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1}};
            int[][] yKernel = {{-1, -2, -1}, {0, 0, 0}, {1, 2, 1}};

            long[] times = new long[EXPS];

            for(int n = 1; n <= EXPS; n++){

                 long startTime = System.currentTimeMillis(); //get the start time to calculate execution time
                
                 SobelConcurrency.filter(pixelData, xKernel, yKernel);
                 
                 long executionTime = System.currentTimeMillis() - startTime; //get the finish time and calculate the execution time
                 System.out.println("Exp " + n + ":     " + (executionTime /1000.0) + " s");
                
                 times[n - 1] = executionTime;   
            }

            double[] stats = SobelConcurrency.genStats(times);
            System.out.println("Average Execution time: " + String.format("%.3f", stats[0]) + " s");
            System.out.println("Standard Deviation: " + String.format("%.2f", stats[1]));

            int[][] output = SobelConcurrency.filter(pixelData, xKernel, yKernel);

            BufferedImage result = arrayToBuf(output, image.getWidth() - 1, image.getHeight() - 1);
			File imageFile = new File("filtered.png");
			ImageIO.write(result, "PNG", imageFile);
        }catch (IOException e) {
			e.printStackTrace();
        }
    }

	/*
	* This method calculate the filter
	* @param pixelData array with the pixels data
	* @return returns return the filter resulte 
	*/
	public static int[][] filter(int[][] pixelData, int[][] xKernel, int[][] yKernel){
             int[][] result = new int[pixelData.length][pixelData[0].length];
            
        try{
            KernelThread tgx = new KernelThread(pixelData, xKernel);
            KernelThread tgy = new KernelThread(pixelData, yKernel);

            long startTime = System.currentTimeMillis();

            tgx.start();
            tgy.start();

            tgx.join();
            tgy.join();

            for (int i = 1; i < pixelData.length - 1; i++) {
                for (int j = 1; j < pixelData[i].length - 1; j++) {
                    result[i][j] = (int) Math.sqrt((tgx.result[i][j] * tgx.result[i][j]) + (tgy.result[i][j] * tgy.result[i][j]));
                }
            }
        }catch(InterruptedException e) {
            System.out.println("Thread interrupted.");
        }

        return result;
    }


    /*
     * This method store from buffer to array the RGB code from each pixel
     * @param BufferedImage
     * @return pixelData array with pixels data
     */
    public static int[][] bufToArray(BufferedImage image){
		
		int[][] pixelData = new int[image.getWidth()][image.getHeight()];
		Color c;
		
        for (int i = 0; i < pixelData.length; i++) {
			for (int j = 0; j < pixelData[i].length; j++) {
				c = new Color(image.getRGB(j, i));
				pixelData[i][j] = c.getRed();
			}
		}
		return pixelData;
	}


    /*
     * This method store from array to buffer each image pixel data (RGB code) 
     * @param pixelData, Width, Heigth
     * @return result result the buffered image
     */
    public static BufferedImage arrayToBuf(int[][] pixelData, int width, int height){
		
        BufferedImage result = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		int color;
		int value;
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				value = pixelData[i][j];
				color = (255 << 24) | (value << 16) | (value << 8) | value;
				result.setRGB(j, i, color);
			}
		}		
		return result;
	}

/*
This function calcutates the average of executions times in the
experiment and the standard deviation.

@param times  Array containg the execution times in milliseconds the experiment.
returns: Array with 2 elements. The first element being the 
the average of execution times in seconds and second element the standard
deviation.
*/   
    private static double[] genStats(long[] times){
        double avg = 0.0;
        double sum = 0.0;
        double stdDev = 0.0;
        double[] stats = new double[2];

        for(int n = 0; n < EXPS; n++) avg+= times[n];
        stats[0] = ((avg / EXPS) / 1000.0);

        for(int n = 0; n < EXPS; n++) sum += ( (times[n] - avg) * (times[n] - avg) );
        stats[1] = Math.sqrt(sum / (EXPS - 1)) / 1000.0;

        return stats;
    }
}


