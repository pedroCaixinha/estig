import java.lang.Thread;

public class KernelThread extends Thread {

    int[][] pixelData;
    int[][] kernelData;
    int[][] result;

    
    /*
     * class constructor
     * @param pixelData, kernelData both are array
     */
    public KernelThread(int[][] pixelData, int[][] kernelData){

        this.pixelData = pixelData;
        this.result = new int[pixelData.length][pixelData[0].length];
        this.kernelData = kernelData;
    }


   /*
    * This method runs the code
    */
    @Override
    public void run(){

        for (int i = 1; i < pixelData.length - 1; i++) {
			for (int j = 1; j < pixelData[i].length - 1; j++) {

                this.result[i][j] = ((this.kernelData[0][0] * this.pixelData[i-1][j-1]) +
                       (this.kernelData[0][1] * this.pixelData[i][j-1]) +
                       (this.kernelData[0][2] * this.pixelData[i+1][j-1]) +
                       (this.kernelData[1][0] * this.pixelData[i-1][j]) +
                       (this.kernelData[1][1] * this.pixelData[i][j]) +
                       (this.kernelData[1][2] * this.pixelData[i+1][j]) +
                       (this.kernelData[2][0] * this.pixelData[i-1][j+1]) +
                       (this.kernelData[2][1] * this.pixelData[i][j+1]) +
                       (this.kernelData[2][2] * this.pixelData[i+1][j+1]));
            }
        }
    }
}
