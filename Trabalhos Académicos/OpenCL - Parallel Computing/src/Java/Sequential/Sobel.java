/*
Sobel Edge Detection Filter

Authors: Pedro Caixinha 4437 and Miguel Rosa 6219
Last Modification Date: 12/06/2016 

This program implements an Edge Detection Algorithm 
using the Sobel Operator.
The algorithm is applied to a 3620x3620 grayscale image.
*/

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.Color;
import java.lang.Math;

public class Sobel {

    final static int EXPS = 50;
    
     /*
     * Main method
     */
    public static void main(String[] args) {
	    BufferedImage image;
        
	try {
            image = ImageIO.read(new File("../../Images/Lenna.png"));

            int[][] pixelData = Sobel.bufToArray(image);
            
            int[][] xKernel = {{-1, 0, 1}, {-2, 0, 2}, {-1, 0, 1}};
            int[][] yKernel = {{-1, -2, -1}, {0, 0, 0}, {1, 2, 1}};

            long[] times = new long[Sobel.EXPS];

            for(int n = 1; n <= Sobel.EXPS; n++){

               long startTime = System.currentTimeMillis(); //get the start time to calculate execution time
               
               Sobel.filter(pixelData, xKernel, yKernel);
            
               long executionTime = System.currentTimeMillis() - startTime; //get the finish time and calculate the execution time
               System.out.println("Exp " + n + ":     " + (executionTime /1000.0) + " s");
               
               times[n - 1] = executionTime;   
            }
	    
            double[] stats = Sobel.genStats(times);
            System.out.println("Average Execution time: " + String.format("%.3f", stats[0]) + " s");
            System.out.println("Standard Deviation: " + String.format("%.2f", stats[1]) );

            int[][] output = Sobel.filter(pixelData, xKernel, yKernel);
            BufferedImage result = arrayToBuf(output, image.getWidth() - 1, image.getHeight() - 1);
	    File imageFile = new File("filtered.png");
	    ImageIO.write(result, "PNG", imageFile);
        }catch (IOException e) {
	    e.printStackTrace();
        }
    }

	/*
	* This method store from buffer to array the RGB code from each pixel
	* @param BufferedImage
        * @return pixelData
	*/
	public static int[][] bufToArray(BufferedImage image){
		
		int[][] pixelData = new int[image.getWidth()][image.getHeight()]; //New array with image Width and Height
		Color c;
	
		//for each pixel, store on array the RGB code from pixel	
        	for (int i = 0; i < pixelData.length; i++) {
			for (int j = 0; j < pixelData[i].length; j++) {
				c = new Color(image.getRGB(j, i));
				pixelData[i][j] = c.getRed();
			}
		}

		return pixelData; //return the full array
	}


    /*
     * This method store from array to buffer each image pixel data (RGB Color)
     * @param pixelData, width, height
     * @return result result the buffered Image
     */
    public static BufferedImage arrayToBuf(int[][] pixelData, int width, int height){
		
        BufferedImage result = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		int color;
		int value;
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				value = pixelData[i][j];
				color = (255 << 24) | (value << 16) | (value << 8) | value;
				result.setRGB(j, i, color);
			}
		}		
		return result;
	}


    /*
     *This method calculate the filter function and also calculate the execution time
     * @param pixelData array pixelData
     * @return result
     */
    public static int[][] filter(int[][] pixelData, int[][] xKernel, int[][] yKernel){
        
       int[][] result = new int[pixelData.length][pixelData[0].length];
      
       for (int i = 1; i < pixelData.length - 1; i++) {
	 for (int j = 1; j < pixelData[i].length - 1; j++) {

	   int gX = ((xKernel[0][0] * pixelData[i-1][j-1]) +
                     (xKernel[0][1] * pixelData[i][j-1]) +
                     (xKernel[0][2] * pixelData[i+1][j-1]) +
                     (xKernel[1][0] * pixelData[i-1][j]) +
                     (xKernel[1][1] * pixelData[i][j]) +
                     (xKernel[1][2] * pixelData[i+1][j]) +
                     (xKernel[2][0] * pixelData[i-1][j+1]) +
                     (xKernel[2][1] * pixelData[i][j+1]) +
                     (xKernel[2][2] * pixelData[i+1][j+1]));

            int gY = ((yKernel[0][0] * pixelData[i-1][j-1]) +
                      (yKernel[0][1] * pixelData[i][j-1]) +
                      (yKernel[0][2] * pixelData[i+1][j-1]) +
                      (yKernel[1][0] * pixelData[i-1][j]) +
                      (yKernel[1][1] * pixelData[i][j]) +
                      (yKernel[1][2] * pixelData[i+1][j]) +
                      (yKernel[2][0] * pixelData[i-1][j+1]) +
                      (yKernel[2][1] * pixelData[i][j+1]) +
                      (yKernel[2][2] * pixelData[i+1][j+1]));

             result[i][j] = (int) Math.sqrt((gX * gX) + (gY * gY));
         }
      }
      return result;
    }

/*
This function calcutates the average of executions times in the
experiment and the standard deviation.

@param times  Array containg the execution times in milliseconds the experiment.
returns: Array with 2 elements. The first element being the 
the average of execution times in seconds and second element the standard
deviation.
*/   
    private static double[] genStats(long[] times){
        double avg = 0.0;
        double sum = 0.0;
        double stdDev = 0.0;
        double[] stats = new double[2];

        for(int n = 0; n < Sobel.EXPS; n++) avg+= times[n];
        stats[0] = ((avg / Sobel.EXPS) / 1000.0);

        for(int n = 0; n < Sobel.EXPS; n++) sum += ( (times[n] - avg) * (times[n] - avg) );
        stats[1] = Math.sqrt(sum / (Sobel.EXPS - 1)) / 1000.0;

        return stats;
    }
}
