#include <stdio.h>
#include <stdlib.h>
#include "sobel.h"
#include <time.h>
#include <math.h>

#define EXPS 50

int* runKernel(int* input, 
	       cl_program program, 
	       cl_context context, 
	       cl_command_queue command_queue){
  
  /* Creation of the kernel */
  cl_int err;
  char kernel_name[] = "sobel";
  cl_kernel kernel = clCreateKernel(program, kernel_name, &err);
   
  if(err < 0){
    perror("Couldn't create the kernel");
    exit(1);      
  }

  // Number of processing elements
  int NGlobal = 4096*4096; 
  int NLocal = 16;

  int* output = malloc(sizeof(int) * NGlobal);
  
  cl_mem input_cl = clCreateBuffer(context, 
				      CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, 
				      sizeof(int) * NGlobal, input, NULL);
  cl_mem output_cl = clCreateBuffer(context, 
				      CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR,
				      sizeof(int) * NGlobal, output, NULL);
  
  err = clSetKernelArg(kernel, 0, sizeof(cl_mem), &input_cl);
  if(err != CL_SUCCESS){
    perror("Couldn't pass arguments");
    exit(1);      
  }

  err = clSetKernelArg(kernel, 1, sizeof(cl_mem), &output_cl);
  if(err != CL_SUCCESS){
    perror("Couldn't pass arguments");
    exit(1);      
  }

  size_t globalWorkSize[2] = { 4096, 4096 };
  size_t localWorkSize[2] = { NLocal, NLocal }; 

  /* Experiment */ 
  int n;
  double avg, sum, stdDev = 0.0;
  clock_t start, end;
  double* times = malloc(sizeof(double) * EXPS);

  for(n = 1; n <= EXPS; n++){
     
     start = clock();
     // Queue the kernel up for execution across the array
     err = clEnqueueNDRangeKernel(command_queue, 
  			          kernel, 2, 
			          NULL, globalWorkSize, 
			          localWorkSize, 0, NULL, NULL);
     end = clock();
     
     times[n - 1] = (((double) (end - start)) / CLOCKS_PER_SEC );    
     printf("Exp: %d    Execution Time: %fs\n", n, ((double) (end - start)) / CLOCKS_PER_SEC );

     if (err != CL_SUCCESS){
        perror("Couldn't execute");
        exit(1);      
     } 
  }

  /* End of Experiment */
  
  // Calculate average of execution times
  for(n = 0; n < EXPS; n++) avg+= times[n];
  avg /= EXPS;
 
  // Calculate standard deviation
  for(n = 0; n < EXPS; n++) sum += ( (times[n] - avg) * (times[n] - avg) );
  stdDev = sqrt(sum / (EXPS - 1));
 
  printf("Average Execution Time: %fs\n", avg);
  printf("Standard Deviation: %f\n", stdDev);

  free(times);
 
  // Read back results
  err = clEnqueueReadBuffer(command_queue, output_cl, 
			    CL_TRUE, 0, sizeof(int) * NGlobal, 
			    output, 0, NULL, NULL);
  
  clReleaseMemObject(input_cl);
  clReleaseMemObject(output_cl);
  clReleaseKernel(kernel);
  
  return output;
}
