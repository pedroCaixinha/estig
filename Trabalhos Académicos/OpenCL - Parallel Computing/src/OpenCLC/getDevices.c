#include "sobel.h"

cl_device_id* getDevices(cl_platform_id *platforms){
  
  cl_device_id* devices = (cl_device_id*) malloc(sizeof(cl_device_id));
  
  // Select the device
  cl_int err;
  
  err = clGetDeviceIDs(platforms[0], CL_DEVICE_TYPE_GPU, 1, devices, NULL);
  
  if(err < 0) {
    perror("Couldn't find any device.");
    exit(1);
  }

  // Get the device name
  char* device_name;
  size_t device_name_size[1];
  
  err = clGetDeviceInfo(devices[0], CL_DEVICE_NAME, NULL, device_name, &device_name_size);
  
  if(err < 0) {
    perror("Couldn't find any device.");
    exit(1);
  }
  
  device_name = (char*) malloc(sizeof(char) * device_name_size[0]);
  
  err = clGetDeviceInfo(devices[0], CL_DEVICE_NAME, device_name_size[0], device_name, &device_name_size);
  
  if(err < 0) {
    perror("Couldn't find any device.");
    exit(1);
  }
  
  printf("%s\n", device_name);
  
  return devices;
}
