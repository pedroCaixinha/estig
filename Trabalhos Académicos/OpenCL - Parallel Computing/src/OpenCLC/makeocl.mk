OBJS=sobel.o \
     getPlatforms.o\
     getDevices.o\
     buildProgram.o\
     runKernel.o

# no caso duma plataforma AMD
#-I $(AMDAPPSDKROOT)/include
# no caso duma plataforma NVIDIA
#-I/usr/local/cuda/include
CFLAGS=-g -O2 -fPIC `pkg-config --cflags opencv` \
        -I/usr/local/cuda/include

# no caso duma plataforma AM
#          -L $(AMDAPPSDKROOT)/lib/x86_64 -l OpenCL	
# no caso duma plataforma NVIDIA
#-L/usr/local/cuda/lib64 -l OpenCL	
LDFLAGS = `pkg-config --libs opencv` \
          -L/usr/local/cuda/lib64 -l OpenCL -lm	


all: sobel

getPlatforms.o: getPlatforms.c sobel.h
getDevices.o: getDevices.c sobel.h
buildProgram.o: buildProgram.c sobel.h
runKernel.o: runKernel.c sobel.h

sobel: $(OBJS) sobel.h
	$(CC) $(OBJS) -o sobel $(LDFLAGS)

clean:
	rm *.o
