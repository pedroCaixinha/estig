#ifndef __SOBEL
#define __SOBEL 1
#include <CL/cl.h>

extern cl_platform_id* getPlatforms();

extern cl_device_id* getDevices(cl_platform_id *platforms);

extern cl_program buildProgram(char** file_name, int NUM_FILES, cl_device_id* devices, cl_context context);

extern int* runKernel(int* srcImage, cl_program program, cl_context context, cl_command_queue command_queue);

#endif
