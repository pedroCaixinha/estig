#include <stdio.h>
#include "sobel.h"

cl_program buildProgram(char** file_name, int NUM_FILES, cl_device_id* devices, cl_context context){
  
  cl_program program; //OpenCL program object

  // Read program file
  FILE* program_handle;
  char* program_buffer[NUM_FILES];
  char* program_log;
  //const char* file_name[] = {PROGRAM_FILE_1};
  
  size_t program_size[NUM_FILES];
  size_t log_size;
  int i;
  cl_int err;
  printf("NUM FILES = %d\n", NUM_FILES);
  printf("FILE NAME = %s\n", file_name[0]);

  for(i = 0; i < NUM_FILES; i++){
    program_handle = fopen(file_name[i], "r");
    if(program_handle == NULL){
      perror("couldn't find the program file");
      exit(1);
    }
    
    // Get size of kernel source
    fseek(program_handle, 0, SEEK_END);
    program_size[i] = ftell(program_handle);
    rewind(program_handle);
    printf("tamanho do programa = %d\n", program_size[i]);
    
    // Read the kernel source into buffer
    program_buffer[i] = (char*) malloc(program_size[i]+1);
    program_buffer[i][program_size[i]] = '\0';
    fread(program_buffer[i], sizeof(char), 
	  program_size[i], program_handle);
  
    fclose(program_handle);
  }
 
  // Create program from buffer
  program = clCreateProgramWithSource(context, NUM_FILES, (const char**) program_buffer, program_size, &err);
  if(err < 0){
    perror("Couldn't create the program");
    exit(1);      
  }

  err = clBuildProgram(program, 1, &devices[0], NULL, NULL, NULL);
  if(err < 0){
    perror("Couldn't build the kernel\n");

    clGetProgramBuildInfo(program, &devices[0], CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
    program_log = (char*)malloc(log_size+1);
    program_log[log_size] = '\0';
    clGetProgramBuildInfo(program, &devices[0], CL_PROGRAM_BUILD_LOG, log_size+1, program_log, NULL);
    printf("%s\n", program_log);
    free(program_log);
    exit(1);
  }

  for(i=0; i<NUM_FILES; i++) {
    free(program_buffer[i]);
  }

  return program;
}

