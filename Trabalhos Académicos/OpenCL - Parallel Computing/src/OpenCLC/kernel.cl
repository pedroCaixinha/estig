__kernel void sobel(__global int *input, __global int *output){
	
	uint x = get_global_id(0);      // Row
        uint y = get_global_id(1);      // Col
        
	uint width = get_global_size(0);   // Width
  	uint height = get_global_size(1);  // Height

	float gX = 0.0;  
        float gY = 0.0;

	if( x >= 1 && x < (width-1) && y >= 1 && y < height - 1) {
          float i00 = input[(x - 1) + (y - 1) * width];
          float i10 = input[x + (y - 1) * width];
          float i20 = input[(x + 1) + (y - 1) * width];
          float i01 = input[(x - 1) + y * width];
          float i11 = input[x + y * width];
          float i21 = input[(x + 1) + y * width];
          float i02 = input[(x - 1) + (y + 1) * width];
          float i12 = input[x + (y + 1) * width];
          float i22 = input[(x + 1) + (y + 1) * width];

          gX = i00 + 2 * i10 + i20 - i02  - 2 * i12 -i22;
          gY = i00 - i20  + 2 *i01 - 2 *i21 + i02  -  i22;

          output[x + y * width] = min((uint)255, (uint)sqrt((gX * gX) + (gY * gY)));	
	
	}
}
