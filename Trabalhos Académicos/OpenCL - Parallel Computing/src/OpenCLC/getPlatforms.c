#include "sobel.h"

cl_platform_id* getPlatforms(){
  /* OpenCL Operations*/
  // Get Platforms
  cl_platform_id *platforms; 
  cl_uint num_platforms;
  cl_int i, err, platform_index = -1;

  // Get number of platforms
  err = clGetPlatformIDs(1, NULL, &num_platforms);
  if(err < 0) {
    perror("Couldn't find any platforms.");
    exit(1);
  }
  printf("Number of available platforms:  %d\n", num_platforms);
  
  platforms = (cl_platform_id*) malloc(sizeof(cl_platform_id) * num_platforms);

  clGetPlatformIDs(num_platforms, platforms, NULL);
  
  // Get platform name
  char* platform_name;
  size_t platform_name_size[1];
  err = clGetPlatformInfo(platforms[0], CL_PLATFORM_NAME, (size_t) NULL, platform_name, &platform_name_size);
  
  if(err < 0) {
    perror("Couldn't find any platforms.");
    exit(1);
  }

  platform_name = (char*) malloc(sizeof(char) * platform_name_size[0]);
  
  err = clGetPlatformInfo(platforms[0], CL_PLATFORM_NAME, platform_name_size[0], platform_name, &platform_name_size);
  
  if(err < 0) {
    perror("Couldn't find any platforms.");
    exit(1);
  }
  
  printf("%s\n", platform_name);

  return platforms;
}
