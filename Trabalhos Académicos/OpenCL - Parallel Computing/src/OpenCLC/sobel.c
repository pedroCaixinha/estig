/*
Sobel Edge Detection Filter

Authors: Pedro Caixinha 4437 and Miguel Rosa 6219
Last Modification Date: 12/06/2016 

This program implements an Edge Detection Algorithm 
using the Sobel Operator.
The algorithm is applied to a 3620x3620 grayscale image.
*/
#include <cv.h>    
#include <stdio.h>
#include <stdlib.h>
#include "sobel.h"

#define NUM_FILES 1                // Number of program files
#define PROGRAM_FILE "kernel.cl"   // Program file name
#define IMAGE_WIDTH 3620           // Image Width
#define IMAGE_HEIGHT 3620          // Image Height
#define WORKING_HEIGHT 4096        // Working Image Width
#define WORKING_WIDTH 4096         // Working Image Height
#define ORIGINAL_IMAGE_OFFSET 238  // Original Image Offset

int* readImageData(char*);        // Image reading function
void createFilteredImage(int*);   // Result Image Funtion

/*
This functions reads pixel values from the original
image into an arrayi of dimensions 4096*4096.

filepath: Image file path.
returns: Array containg the pixel values.
*/
int* readImageData(char* filePath){
  
  IplImage* image = cvLoadImage(filePath,0);
  int k1, k2;
  int stride = 4096;
  int* pixelData = malloc(sizeof(int) * WORKING_WIDTH * WORKING_HEIGHT);
  uchar* ptr;
  
  for(k1 = 0; k1 < IMAGE_HEIGHT; k1++){
     ptr = (uchar*) (image->imageData + k1 * image->widthStep);
     for(k2 = 0; k2 < IMAGE_WIDTH; k2++){
        pixelData[((ORIGINAL_IMAGE_OFFSET + k1) * stride) + ORIGINAL_IMAGE_OFFSET + k2] = ptr[k2]; 
     }
  }

  return pixelData;
}

/*
This function creates the result image file.

pixelData: Array containg the pixel values.
*/
void createFilteredImage(int* pixelData){

    int k1, k2;
    int stride = 4096;
    IplImage* filteredImage = cvCreateImage(cvSize(IMAGE_WIDTH, IMAGE_HEIGHT), IPL_DEPTH_8U, 1);
    uchar* ptr;
 
    for(k1 = 0; k1 < IMAGE_HEIGHT; k1++){
       ptr = (uchar*) (filteredImage->imageData + k1 * filteredImage->widthStep);
       for(k2 = 0; k2 < IMAGE_WIDTH; k2++){
          ptr[k2] = pixelData[((ORIGINAL_IMAGE_OFFSET + k1) * stride) + ORIGINAL_IMAGE_OFFSET + k2];
       }
    }
 
    cvSaveImage("filtered.png", filteredImage, 0);
}

/*
Main function.

This function conduts the experiment of applying the Sobel Edge Detection
filter on the image the number of times defined in EXPS.
*/
int main(){
  
  int *srcPixelData = readImageData("../Images/Lenna.png"); 
 
  cl_int err; // Error code

  /* OpenCL Operations*/
  // Get Platforms
  cl_platform_id *platforms = getPlatforms();

  // Get Devices 
  cl_device_id* devices = getDevices(platforms);

  // Create execution context
  cl_context context;
  context = clCreateContext(NULL, 1, &devices[0], NULL, NULL, &err);
  if(err < 0) {
    perror("Couldn't find any device.");
    exit(1);
  }

  // Create execution commmand queue
  cl_command_queue command_queue = clCreateCommandQueue(context, devices[0], 0, &err);
  if(err < 0){
    perror("Couldn't create the command queue.");
    exit(1);      
  }
  
  // Create program
  char* file_name[] = {PROGRAM_FILE};
  
  cl_program program = buildProgram(file_name, NUM_FILES, devices, context);
 
  // Run the program
  int* output = runKernel(srcPixelData, program, context, command_queue);

  /* End of OpenCL Operations */
  
  createFilteredImage(output);
  
  printf("End of operations.\n");

  /* Deallocate resources */
  clReleaseCommandQueue(command_queue);
  clReleaseProgram(program);
  clReleaseContext(context);
  
  free(srcPixelData);
  free(output);

  return 0;
}
