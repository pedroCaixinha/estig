/* Creation of the execution context */

cl_context context;

context = clCreateContext(NULL, 1, &devices[0], NULL, NULL, &err);
  
if(err < 0) {
    perror("Couldn't find any device.");
    exit(1);
}
