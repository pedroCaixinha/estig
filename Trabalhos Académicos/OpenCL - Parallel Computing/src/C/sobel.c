/*
Sobel Edge Detection Filter

Authors: Pedro Caixinha 4437 and Miguel Rosa 6219
Last Modification Date: 12/06/2016 

This program implements an Edge Detection Algorithm 
using the Sobel Operator.
The algorithm is applied to a 3620x3620 grayscale image.
*/

#include <stdio.h>
#include <stdlib.h>
#include <cv.h>
#include <math.h>
#include <time.h>

#define EXPS 50	          // Number of experiments
#define IMAGE_WIDTH 3620  // Image Width
#define IMAGE_HEIGHT 3620 // Image Height

double* genStats(double*);        // Stats function
int* readImageData(char*);        // Image reading function
void applyFilter(int*, int*);     // Filter funtion
void createFilteredImage(int*);   // Result Image Funtion

/*
This function creates the result image file.

pixelData: Array containg the pixel values.
*/
void createFilteredImage(int* pixelData){

    int k1, k2;
  
    IplImage* filteredImage = cvCreateImage(cvSize(IMAGE_WIDTH - 1, IMAGE_HEIGHT - 1), IPL_DEPTH_8U, 1);
    uchar* ptr;
 
    for(k1 = 0; k1 < IMAGE_HEIGHT; k1++){
       ptr = (uchar*) (filteredImage->imageData + k1 * filteredImage->widthStep);
       for(k2 = 0; k2 < IMAGE_WIDTH; k2++){
          ptr[k2] = pixelData[k1 * IMAGE_WIDTH + k2];
       }
    }

    cvSaveImage("filtered.png", filteredImage, 0);
}

/*
This function apply the Sobel operator.

input: Array containg the original pixel values.
output: Array containg the modified pixel values.
*/
void applyFilter(int* input, int* output){
   
   int k1, k2;
   float gX = 0.0;
   float gY = 0.0;

   for(k1 = 1; k1 < IMAGE_HEIGHT - 1; k1++) {
      for(k2 = 1; k2 < IMAGE_WIDTH - 1; k2++) {	
         float i00 = input[(k1 - 1) + (k2 - 1) * IMAGE_WIDTH];
         float i10 = input[k1 + (k2 - 1) * IMAGE_WIDTH];
         float i20 = input[(k1 + 1) + (k2 - 1) * IMAGE_WIDTH];
         float i01 = input[(k1 - 1) + k2 * IMAGE_WIDTH];
         float i11 = input[k1 + k2 * IMAGE_WIDTH];
         float i21 = input[(k1 + 1) + k2 * IMAGE_WIDTH];
         float i02 = input[(k1 - 1) + (k2 + 1) * IMAGE_WIDTH];
         float i12 = input[k1 + (k2 + 1) * IMAGE_WIDTH];
         float i22 = input[(k1 + 1) + (k2 + 1) * IMAGE_WIDTH];

         gX = i00 + 2 * i10 + i20 - i02  - 2 * i12 -i22;
         gY = i00 - i20  + 2 *i01 - 2 *i21 + i02  -  i22;
         
         output[k1 + k2 * IMAGE_WIDTH] = fmin(255, (uint) sqrt((gX * gX) + (gY * gY)));
      }
   }
}

/*
This functions reads pixel values from the original
image into an array.

filepath: Image file path.
returns: Array containg the pixel values.
*/
int* readImageData(char* filePath){
  
  IplImage* image = cvLoadImage(filePath,0);
  int k1, k2;
  int* pixelData = malloc(sizeof(int) * IMAGE_WIDTH * IMAGE_HEIGHT);
  uchar* ptr;
  
  for(k1 = 0; k1 < IMAGE_HEIGHT; k1++){
     ptr = (uchar*) (image->imageData + k1 * image->widthStep);
     for(k2 = 0; k2 < IMAGE_WIDTH; k2++){
        pixelData[k1 * image->widthStep + k2] = ptr[k2]; 
     }
  }
 
  return pixelData;
}

/*
This function calcutates the average of executions times in the
experiment and the standard deviation.

times: Array containg the execution times in the experiment.
returns: Array with 2 elements. The first element being the 
the average of execution times and second element the standard
deviation.
*/
double* genStats(double* times){
    double* stats = malloc(sizeof(double) * 2);
    int n;
    double avg, sum, stdDev = 0.0;
    
    for(n = 0; n < EXPS; n++) avg+= times[n];
    stats[0] = avg / EXPS;

    for(n = 0; n < EXPS; n++) sum += ( (times[n] - avg) * (times[n] - avg) );
    stats[1] = sqrt(sum / (EXPS - 1));
    
    return stats;
}

/*
Main function.

This function conduts the experiment of applying the Sobel Edge Detection
filter on the image the number of times defined in EXPS.
*/
int main(){
  
    int *srcPixelData = readImageData("../Images/Lenna.png"); 
    int *dstPixelData;    

    clock_t start, end;
    int n;
    double* times = malloc(sizeof(double) * EXPS);

    for(n = 1; n <= EXPS; n++){

       dstPixelData = malloc(sizeof(int) * IMAGE_WIDTH * IMAGE_HEIGHT);
 
       start = clock();
       applyFilter(srcPixelData, dstPixelData);
       end = clock();

       times[n - 1] = (((double) (end - start)) / CLOCKS_PER_SEC );    
       printf("Exp: %d    Execution Time: %fs\n", n, ((double) (end - start)) / CLOCKS_PER_SEC );
    }
  
    double* stats = genStats(times);

    printf("Average Execution Time: %.3fs\n", stats[0]);
    printf("Standard Deviation: %.2f\n", stats[1]);
    
    createFilteredImage(dstPixelData);
      
    free(srcPixelData);
    free(times); 
 
    return 0;
}
