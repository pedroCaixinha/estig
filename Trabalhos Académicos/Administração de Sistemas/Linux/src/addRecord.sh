#!/bin/bash

# Author: Pedro Caixinha 4437
# Date: 25-04-2016
#
# This script provides the following functionality:
# - Creation of type A and MX DNS resource records in a given zone file.

# User interaction
read -p "Please enter the domain name: " domainName

# This function inserts an A record in specified domain zone file.
function insertARecord {
	printf "$1\tIN\tA\t$2\n" > rc.temp
        cat rc.temp >> /var/named/$domainName.hosts
}

# This function is responsible for the creation a A resource record
function createARecord {
	read -p "Please enter the destination IP Address: " destinationIP
	read -p "Please enter a record name: (Example: www, smtp, pop)" recordName
	
	insertARecord $recordName $destinationIP

	/etc/init.d/named restart

	break
}

# This function is responsible for the creation a MX resource record
function createMXRecord {
	read -p "Please enter the destination IP Address: " destinationIP
        read -p "Please enter the FQDN: (Example: mail.test.com)" fqdn	
	
	host=$(echo $fqdn | cut -d'.' -f 1)
	insertARecord $host $destinationIP

	printf "\tIN\tMX\t10\t$fqdn.\n" > rc.temp
        cat rc.temp >> /var/named/$domainName.hosts
	
	/etc/init.d/named restart

        break
}

# This control structure contains the main stream of the script.
if [[ -z "$domainName" ]]; then
        cat <<- EOM 
        You must provide a domain name!
	EOM
else
	if [ ! -f /var/named/${domainName}.hosts ]; then
    		echo "Could not find the file /var/named/${domainName}.hosts"
	else
		echo "Please select the type of record you wish to create:"
        	select option in "A" "MX" "Abort"
        	do
                	case $option in
                        	A) createARecord;;
                        	MX) createMXRecord;;    
                       		Abort) break;;
                        	*) echo "Invalid option!";;     
                	esac
        	done

	fi
fi
