#!/bin/bash

# Author: Pedro Caixinha 4437
# Date: 25-04-2016
#
# This script provides the following functionality:
# - Creation of a reverse zone for a domain name specified by the user.

# User interaction
read -p "Please enter the FQDN (Example: foo.bar.com): " fqdn
read -p "Please enter the IP Address: " ip

# This function writes the master zone information in /etc/named.conf
function createMasterZone {

        zone=$(sed -n "/zone \"${zoneName}\"/p" /etc/named.conf)
	
        if [[ ! -z "$zone" ]]; then
                sed -i "/zone \"${zoneName}\"/,/^};/d" /etc/named.conf
                createMasterZone
        else
		zoneRef=$(sed -n "/DOMAIN ${fqdn}/p" /etc/named.conf)

		if [[ ! -z "$zoneRef" ]]; then
			printf "zone \"$zoneName\" IN {\n\ttype master;\n\tfile \"${zoneName}.hosts\";\n};\n" > zoneTempFile.temp
			sed -i --follow-symlinks "/DOMAIN ${fqdn}/r zoneTempFile.temp" /etc/named.conf
			rm -f zoneTempFile.temp
		else
			printf "// DOMAIN $fqdn\n\nzone \"$zoneName\" IN {\n\ttype master;\n\tfile \"${zoneName}.hosts\";\n};\n" > zoneTempFile.temp
			zoneMark=$(sed -n "/include \"\/etc\/named.rfc1912.zones\";/=" /etc/named.conf)
			zoneWriteLocation=$((zoneMark - 1))
			sed -i --follow-symlinks "${zoneWriteLocation}r zoneTempFile.temp" /etc/named.conf
			rm -f zoneTempFile.temp
		fi
        fi
}

# This function generates zone name
function genZoneName {
	zoneName=""
        dot="."

        for i in {3..1}
        do
                octet=$(echo $ip | cut -d'.' -f $i)
                zoneName="$zoneName$octet$dot"
        done

        zoneName="${zoneName}in-addr.arpa"
}

# This function creates the corresponding reverse zone file in /var/named/
function createReverseZoneFile {

        if [ ! -f /var/named/${zoneName}.hosts ]; then
                touch ${zoneName}.hosts
                echo "\$ttl 38400 " >> /var/named/${zoneName}.hosts
                echo "@       IN      SOA     ${fqdn}. mail.${fqdn}. (" >> /var/named/${zoneName}.hosts
                echo "                  1165190726 ; serial" >> /var/named/${zoneName}.hosts
                echo "                  10800 ; refresh" >> /var/named/${zoneName}.hosts
                echo "                  3600 ; retry" >> /var/named/${zoneName}.hosts
                echo "                  604800 ; expire" >> /var/named/${zoneName}.hosts
                echo "                  38400 ; minimum" >> /var/named/${zoneName}.hosts
                echo "                  )" >> /var/named/${zoneName}.hosts
                echo "	IN      NS      ${fqdn}." >> /var/named/${zoneName}.hosts
                echo "1	IN      PTR	${fqdn}." >> /var/named/${zoneName}.hosts
                echo "1	IN      PTR	www.${fqdn}." >> /var/named/${zoneName}.hosts
        else
                cp /var/named/${zoneName}.hosts /var/named/${zoneName}.hosts.bak
                rm -f /var/named/${zoneName}.hosts
                createReverseZoneFile
        fi
}

# This control structure contains the main stream of the script.
if [[ -z "$fqdn" || -z "$ip" ]]; then
        cat <<- EOM 
        You must provide a FQDN name and IP Address!
	EOM
else
	genZoneName
	createMasterZone
	createReverseZoneFile
	/etc/init.d/named restart	
fi

