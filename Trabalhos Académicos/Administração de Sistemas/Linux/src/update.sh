#!/bin/bash

# Author: Pedro Caixinha 4437
# Date: 26-04-2016
#
# This script provides the following functionality:
# - Update/Deletion of type A and MX DNS resource records in a given forward zone file.

# User interaction
read -p "Please enter the domain name (Example: test.com): " domainName

# This function is responsible for deleting a resource record
function deleteRecord {
	sed -i "0,/${records[$recordIndex]}/s//DELETE/" /var/named/${domainName}.hosts
	sed -i "/DELETE/d" /var/named/${domainName}.hosts
	printf "Operation complete succefully.\nChanges were made to /var/named/${domainName}.hosts\n"
	break
}

# This function is responsible for updating a resource record
function updateRecord {
	printf "Please enter the new record string.\nExample: webmail	IN	A	10.11.12.13\nNote: For a better format in the file use tabs between parameters.\n"
	read -p ">" recordString
	sed -i "0,/${records[$recordIndex]}/s//$recordString/" /var/named/${domainName}.hosts
	printf "Operation complete succefully.\nChanges were made to /var/named/${domainName}.hosts\n"
        break
}

# This control structure contains the main stream of the script.
if [[ -z "$domainName" ]]; then
        cat <<- EOM 
        You must provide a domain name!
	EOM
else
	if [ ! -f /var/named/${domainName}.hosts ]; then
    		echo "Could not find the file /var/named/${domainName}.hosts"
	else
        	sed -n '/NS/,$w temp.0' /var/named/${domainName}.hosts
		nRecords=$(cat ./temp.0 | wc -l)
		records=()

		while read record           
		do          
			records+=("$record")
		done < temp.0  
		
		for (( i=0; i< (( $nRecords )); i++ ))
		do
			printf "$i) ${records[$i]}\n"
		done

		read -p "Please select the record you wish to update: " recordIndex
		while [ "$recordIndex" -lt 1 ] || [ "$recordIndex" -gt "$nRecords" ]
		do
			read -p "Invalid Option!" recordIndex
		done

		echo "Please select the type of operation you would like to perform:"
		select option in "Update_Record" "Delete_Record" "Abort"
        	do
                	case $option in
                        	Update_Record) updateRecord;;
                        	Delete_Record) deleteRecord;;    
                       		Abort) break;;
                        	*) echo "Invalid option!";;     
                	esac
        	done
	fi
fi
