#!/bin/bash

# Author: Pedro Caixinha 4437
# Date: 24-04-2016
#
# This script provides the following functionality:
# - Creation of a master zone for a domain name specified by the user.
# - Creation of the corresponding forward zone file.
# - Creation of a VirtualHost for the specified domain. 

# User interaction
read -p "Please enter the domain name: " domainName
read -p "Please enter the DNS Server IP Address: " dnsServerIP

# This function writes the master zone information in /etc/named.conf
function createMasterZone {

	zoneRef=$(sed -n "/zone \"${domainName}\"/p" /etc/named.conf)

	if [[ ! -z "$zoneRef" ]]; then
		sed -i "/zone \"${domainName}\"/,/^};/d" /etc/named.conf
		createMasterZone
	else
		domainRef=$(sed -n "/DOMAIN ${domainName}/p" /etc/named.conf)
		
		if [[ ! -z "$domainRef" ]]; then
                	printf "\nzone \"$domainName\" IN {\n\ttype master;\n\tfile \"/var/named/$domainName.hosts\";\n};\n\n" > zoneTempFile.temp
			sed -i --follow-symlinks "/DOMAIN ${domainName}/r zoneTempFile.temp" /etc/named.conf
                        rm -f zoneTempFile.temp
		else
			zoneMark=$(sed -n "/include \"\/etc\/named.rfc1912.zones\";/=" /etc/named.conf)
	                zoneWriteLocation=$((zoneMark - 1))

        	        printf "//DOMAIN $domainName\n\nzone \"$domainName\" IN {\n\ttype master;\n\tfile \"/var/named/$domainName.hosts\";\n};\n\n" > zoneTempFile.temp

                	sed -i --follow-symlinks "${zoneWriteLocation}r zoneTempFile.temp" /etc/named.conf
                	rm -f zoneTempFile.temp
		fi
	fi
}

# This function creates the corresponding forward zone file in /var/named/
function createForwardZoneFile {

	if [ ! -f /var/named/${domainName}.hosts ]; then
    		touch ${domainName}.hosts
		echo "\$ttl 38400 " >> /var/named/${domainName}.hosts
		echo "@       IN      SOA     ${domainName}. mail.${domainName}. (" >> /var/named/${domainName}.hosts
                echo "			1165190726 ; serial" >> /var/named/${domainName}.hosts
                echo "			10800 ; refresh" >> /var/named/${domainName}.hosts
                echo "			3600 ; retry" >> /var/named/${domainName}.hosts
                echo "			604800 ; expire" >> /var/named/${domainName}.hosts
                echo "			38400 ; minimum" >> /var/named/${domainName}.hosts
                echo "			)" >> /var/named/${domainName}.hosts
	        echo "	IN	NS	${domainName}." >> /var/named/${domainName}.hosts
		echo "	IN      A	$dnsServerIP" >> /var/named/${domainName}.hosts
		echo "www	IN      A	$dnsServerIP" >> /var/named/${domainName}.hosts
	else
		cp /var/named/${domainName}.hosts /var/named/${domainName}.hosts.bak
		rm -f /var/named/${domainName}.hosts
		createForwardZoneFile
	fi	
}

# This function writes the virtual host configuration into /etc/httpd/conf/httpd.conf
function insertVirtualHostConfig {
	virtualHost=$(sed -n "/# VirtualHost ${domainName}/p" /etc/httpd/conf/httpd.conf)
	if [[ ! -z "$virtualHost" ]]; then
	        sed -i "/# VirtualHost ${domainName}/,/^<\/VirtualHost>/d" /etc/httpd/conf/httpd.conf
        
	        insertVirtualHostConfig
        else
		virtualHostMark=$(sed -n "/# VirtualHost example:/=" /etc/httpd/conf/httpd.conf)
                virtualHostWriteLocation=$((virtualHostMark - 1))
		
		printf "# VirtualHost ${domainName}\n\n<VirtualHost $dnsServerIP:80>\n\tDocumentRoot /domains/${domainName}\n\tServerName www.${domainName}\n\tServerAlias ${domainName}\n\t<Directory \"/domains/${domainName}\">\n\t\tOptions Indexes FollowSymLinks\n\t\tAllowOverride All\n\t\tOrder allow,deny\n\t\tAllow from all\n\t</Directory>\n</VirtualHost>\n\n" > virtualHostTempFile.temp
	
		sed -i --follow-symlinks "${virtualHostWriteLocation}r virtualHostTempFile.temp" /etc/httpd/conf/httpd.conf
                rm -f virtualHostTempFile.temp
	fi
}

# This function creates DocumentRoot directory for the virtualhost
# and invokes the function insertVirtualHostConfig which is responsible
# for writing the virtual host configuration into /etc/httpd/conf/httpd.conf 
function createVirtualHost {

	mkdir -p /domains/${domainName}
	chmod 755 /domains/ -R
	echo "Welcome to ${domainName}!" > /domains/${domainName}/index.html
	cp /etc/httpd/conf/httpd.conf /etc/httpd/conf/httpd.conf.bak	
	
	nameVirtualHost=$(sed -n "/NameVirtualHost ${dnsServerIP}:80/p" /etc/httpd/conf/httpd.conf)

	if [[ -z "$nameVirtualHost" ]]; then
                sed -i "/#NameVirtualHost \*:80/a NameVirtualHost ${dnsServerIP}:80" /etc/httpd/conf/httpd.conf
		insertVirtualHostConfig
        else
		insertVirtualHostConfig
	fi
}

# This control structure contains the main stream of the script.
if [[ -z "$domainName" || -z "$dnsServerIP" ]]; then
	cat <<- EOM 
	You must provide a domain name and the IP Address of the DNS server!
	EOM
else
        createMasterZone
	createForwardZoneFile
	/etc/init.d/named restart

	createVirtualHost
	/etc/init.d/httpd restart
fi
