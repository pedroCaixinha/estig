#!/bin/bash

# Author: Pedro Caixinha 4437
# Date: 25-04-2016
#
# This script provides the following functionality:
# - Removal of master zones (forward and reverse)
# - Removal of virtual hosts

# This function handles the process of removing a forward or reverse zone
function removeZone {
	read -p "Please enter the name of the zone (Example: myzone.com, 90.8.79.in-addr.arpa): " zoneName
	
	zoneRef=$(sed -n "/zone \"${zoneName}\"/p" /etc/named.conf)
	sed -n "/zone \"${zoneName}\"/,/^};/w temp.0" /etc/named.conf
	fileString=$(sed -n "/file/p" ./temp.0)
	nChar=${#fileString}
	endChar=$(( nChar -9 ))
	fileName=${fileString:7:$endChar}
        rm -f ./temp.0
	
	if [[ -z "$zoneRef" ]]; then
        	cat <<- EOM
		Could not find any zone records for the domain name $zoneName"
		EOM
	else
		sed -i "/zone \"${zoneName}\"/,/^};/d" /etc/named.conf
                rm -f $fileName
		printf "Operation completed succefully!\nChanges were made to /etc/named.conf\nThe following Zone File was removed: $fileName\n"
		break 
        fi
}

# This function removes the alternate matched record that was found
function removeSuggestedRef {
	sed -i "/# VirtualHost ${newRef}/,/^<\/VirtualHost>/d" /etc/httpd/conf/httpd.conf
	printf "Operation completed succefully.\nChanges were made to /etc/httpd/conf/httpd.conf\n"
	break 2
}

# This function handles the process of removing a virtual host
function removeVirtualHost {
	read -p "Please enter the name of the virtual host (Example: www.mysite.com, mysite.com): " virtualHostName
	
	prefix=${virtualHostName:0:3}

	if [[ "$prefix" == "www" ]];then
		newRef=${virtualHostName:4}
	else
		newRef="www.${virtualHostName}"
	fi

	searchRef1=$(sed -n "/# VirtualHost ${virtualHostName}/p" /etc/httpd/conf/httpd.conf)
	searchRef2=$(sed -n "/# VirtualHost ${newRef}/p" /etc/httpd/conf/httpd.conf)
	
	if [[ -z "$searchRef1" && -z "$searchRef2" ]]; then
		printf "Could not find any records related to ${virtualHostName} in /etc/httpd/conf/httpd.conf!\n"
		break
	elif [[ -z "$searchRef1" && ! -z "$searchRef2" ]]; then
		printf "Could not find any records related to ${virtualHostName} in /etc/httpd/conf/httpd.conf!\n\nHowever the following record was found:\n\n"
		sed -n "/# VirtualHost ${newRef}/,/^<\/VirtualHost>/p" /etc/httpd/conf/httpd.conf
		echo
		echo "Do you wan't to remove this record?"
		select option in "Yes" "No"
		do
        		case $option in
               			Yes) removeSuggestedRef;;
                		No) break 2;;
               			*) echo "Invalid option!";;
        		esac
		done
	else
		sed -i "/# VirtualHost ${virtualHostName}/,/^<\/VirtualHost>/d" /etc/httpd/conf/httpd.conf
		printf "Operation completed succefully.\nChanges were made to /etc/httpd/conf/httpd.conf\n"
		break	
	fi	
}

# This control structure contains the main stream of the script.
echo "Please select the type of operation that you would like to perform: "
select option in "Remove_Zone" "Remove_Virtual_Host" "Quit"
do
	case $option in
        	Remove_Zone) removeZone;;
		Remove_Virtual_Host) removeVirtualHost;;
                Quit) break;;
                *) echo "Invalid option!";;
	esac
done
