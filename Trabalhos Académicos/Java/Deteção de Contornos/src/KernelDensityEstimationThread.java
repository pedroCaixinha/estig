/*
Instituto Politécnico de Beja
Estruturas de Dados e Algoritmos

Autor: Pedro Caixinha
Data: 29/05/2017

Esta classe define objectos do tipo KernelDensityEstimationThread que
definem os fios de execução utilizados na operação de
extração da função densidade de probabilidades.
*/

import java.lang.Thread;

public class KernelDensityEstimationThread extends Thread {

    private static final int GRAY_LEVELS = 256;  // Número de intensidades disponiveis numa escala de cinzentos

    private float[] kde;        // Array que irá armazenar as imagens resultantes da função
    private int[] histogram;    // Array que representa o histograma de intensidades
    private double k1;          // Constante K1 utilizada no cálculo da função
    private double k2;          // Constante K2 utilizada no cálculo da função
    private int start;          // Indice incial de processamento
    private int end;            // Indice final de processamento

    /*
     * Método Construtor
     * Cria um objecto do tipo KernelDensityEstimationThread.
     * @param kde Array que irá armazenar as imagens resultantes da função
     * @param histogram Array que representa o histograma de intensidades
     * @param K1 Constante K1 utilizada no cálculo da função
     * @param K2 Constante K2 utilizada no cálculo da função
     * @param start Indice incial de processamento
     * @param end Indice final de processamento
     */
    public KernelDensityEstimationThread(float[] kde,
                     int[] histogram,
                     double k1,
                     double k2,
                     int start,
                     int end){

        this.kde = kde;
        this.histogram = histogram;
        this.k1 = k1;
        this.k2 = k2;
        this.start = start;
        this.end = end;
    }

    /*
     * Método de execução do fio.
     * Preenche uma parte pré-detereminada do array kde,
     * com os resultados da função.
     */
    @Override
    public void run(){

        for (int x = this.start; x < this.end; x++) {
            double sum = 0.0;

            for(int xi = 0; xi < GRAY_LEVELS; xi++){
                sum += this.histogram[xi] * Math.pow(this.k2, ((x-xi)*(x-xi)));
            }
            this.kde[x] = (float) (this.k1 * sum);
        }
    }
}
