/*
Instituto Politécnico de Beja
Estruturas de Dados e Algoritmos

Autor: Pedro Caixinha
Data: 07/06/2017

Esta classe define objectos do tipo CannyGradientThread que
definem os fios de execução utilizados na fase de obteção dos
gradientes do processo de deteção de contornos segundo o método
de Canny.
*/
import java.lang.Thread;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;

public class CannyGradientThread extends Thread {

    private BufferedImage image;            // Objecto que descreve a imagem através um buffer que permite aceder aos dados da mesma
    private Raster raster;                  // Objecto que descreve a imagem através um buffer que permite aceder aos dados da mesma
    private int filterOffset;               // Dimensão do desfasamento
    private int[][] tempGX;                 // Array bidimensional contendo os resultados da derivada GX resultante da aplicação do operador de Sobel
    private int[][] tempGY;                 // Array bidimensional contendo os resultados da derivada GY resultante da aplicação do operador de Sobel
    private int[][] intensities;            // Array bidimensional que armezena as intensidades de cada pixel da imagem final
    private float[][] gradientDirection;    // Array bidimensional que armazena a direcção do gradiente de cada pixel da imagem opós a convolução com o operador de Sobel
    private int startColFirstPass;          // Indice da coluna de processamento inical para a primeira passagem
    private int endColFirstPass;            // Indice da coluna de processamento final para a primeira passagem
    private int endRowFirstPass;            // Indica da linha de processamento final para a primeira passagem
    private int startColSecondPass;         // Indice da coluna de processamento inical para a segunda passagem
    private int endColSecondPass;           // Indice da coluna de processamento final para a segunda passagem
    private int endRowSecondPass;           // Indica da linha de processamento final para a segunda passagem

    /*
     * Método Construtor
     * Cria um objecto do tipo CannyGradientThread.
     * @param image Objecto que descreve a imagem através um buffer que permite aceder aos dados da mesma
     * @param filterOffset Dimensão do desfasamento
     * @param tempGX Array bidimensional contendo os resultados da derivada GX resultante da aplicação do operador de Sobel
     * @param tempGY Array bidimensional contendo os resultados da derivada GY resultante da aplicação do operador de Sobel
     * @param intensities Array bidimensional que armezena as intensidades de cada pixel da imagem final
     * @param gradientDirection Array bidimensional que armazena a direcção do gradiente de cada pixel da imagem opós a convolução com o operador de Sobel
     * @param startColFirstPass Indice da coluna de processamento inical para a primeira passagem
     * @param endColFirstPass Indice da coluna de processamento final para a primeira passagem
     * @param endRowFisrtPass Indica da linha de processamento final para a primeira passagem
     * @param startColSecondPass Indice da coluna de processamento inical para a segunda passagem
     * @param endColSecondPass Indice da coluna de processamento final para a segunda passagem
     * @param endRowSecondPass Indica da linha de processamento final para a segunda passagem
     */
    public CannyGradientThread (BufferedImage image,
                                int filterOffset,
                                int[][] tempGX,
                                int[][] tempGY,
                                int[][] intensities,
                                float[][] gradientDirection,
                                int startColFirstPass,
                                int endColFirstPass,
                                int endRowFirstPass,
                                int startColSecondPass,
                                int endColSecondPass,
                                int endRowSecondPass){

        this.image = image;
        this.raster = this.image.getData();
        this.filterOffset = filterOffset;
        this.tempGX = tempGX;
        this.tempGY = tempGY;
        this.intensities = intensities;
        this.gradientDirection = gradientDirection;
        this.startColFirstPass = startColFirstPass;
        this.endColFirstPass = endColFirstPass;
        this.endRowFirstPass = endRowFirstPass;
        this.startColSecondPass = startColSecondPass;
        this.endColSecondPass = endColSecondPass;
        this.endRowSecondPass = endRowSecondPass;
    }

    /*
     * Método de execução do fio.
     * Percorre primeiramente a imagem original coluna a coluna e efectua a convolução
     * dos filtros segundo X relativamente a cada pixel, guardando o resultado nos arrays tempGX e
     * tempGY. De seguida, percorre de linha a linha, as intensidades previamente fitradas do
     * arrays tempGX e tempGY e torna a efectuar a convolução mas agora com os filtros segundo Y,
     * guardando o resultado final no array intensities.
     * Cada fio processa uma parte das estruturas de dados na horizontal, com base
     * nos limites estabelecidos.
     */
    @Override
    public void run(){

        int tempGXSum, tempGYSum, gxSum, gySum;

        for (int y = this.filterOffset; y < this.endRowFirstPass; y++) {
            for (int x = this.startColFirstPass; x < this.endColFirstPass; x++) {

                // ALONG X

                tempGXSum = 0;
                tempGYSum = 0;

                tempGXSum += this.raster.getSample(x-1, y, 0);
                tempGXSum += -this.raster.getSample(x+1, y, 0);

                tempGYSum += this.raster.getSample(x-1, y, 0);
                tempGYSum += 2 * this.raster.getSample(x, y, 0);
                tempGYSum += this.raster.getSample(x+1, y, 0);

                this.tempGX[y-this.filterOffset][x-this.filterOffset] = tempGXSum;
                this.tempGY[y-this.filterOffset][x-this.filterOffset] = tempGYSum;
            }
        }

        for (int y = this.filterOffset; y < this.endRowSecondPass; y++) {
            for (int x = this.startColSecondPass; x < this.endColSecondPass; x++) {

                // ALONG Y

                gxSum = 0;
                gySum = 0;

                gxSum += this.tempGX[y-1][x];
                gxSum += 2 * this.tempGX[y][x];
                gxSum += this.tempGX[y+1][x];

                gySum += this.tempGY[y-1][x];
                gySum += -this.tempGY[y+1][x];

                this.intensities[y-this.filterOffset][x-this.filterOffset] = (int) Math.sqrt((gxSum * gxSum) + (gySum * gySum));
                this.gradientDirection[y-this.filterOffset][x-this.filterOffset] = (float) Math.abs(Math.atan2(gySum, gxSum) * 180 / Math.PI);
            }
        }
    }
}
