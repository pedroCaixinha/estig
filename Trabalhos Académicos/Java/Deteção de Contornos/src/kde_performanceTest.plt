# Instituto Politécnico de Beja
# Estruturas de Dados e Algoritmos

# Autor: Pedro Caixinha
# Data: 29/05/2017

# Instruções que permitem ao programa gnuplot gerar para um terminal "postscript eps enhanced color"
# um gráfico representativo dos dados constantes do ficheiro "kde_test.csv"
# Este ficheiro deverá conter os dados resultantes da execução dos testes de medição do desempenho
# computacional da funcionalidade de extração da função densidade de probabilidades.
# O output é direcionado para o ficheiro com o nome "kde_GrowthRate.eps"

set datafile separator ','
set grid

set title "Kernel Density Estimation Execution Times"
set xlabel "Number of Pixeis"
set ylabel "Execution Time (s)"

set term postscript eps enhanced color "Times-Roman,16"
set output 'kde_GrowthRate.eps'

plot 'kde_test.csv' using 1:2 notitle lw 3 lc rgb 'blue' with lines