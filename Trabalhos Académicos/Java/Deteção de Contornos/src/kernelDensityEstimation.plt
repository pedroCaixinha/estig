# Instituto Politécnico de Beja
# Estruturas de Dados e Algoritmos

# Autor: Pedro Caixinha
# Data: 29/05/2017

# Instruções que permitem ao programa gnuplot gerar para um terminal "postscript eps enhanced color"
# um gráfico representativo dos dados constantes do ficheiro "kde_data.csv"
# Este ficheiro deverá conter os dados extraidos da função densidade de probilidades.
# O output é direcionado para o ficheiro com o nome "kde_histogram.eps"

set datafile separator ','
set grid
set style histogram
set title "Histogram of x"
set xlabel "x"
set ylabel "Density"

set term postscript eps enhanced color "Times-Roman,16"
set output 'kde_histogram.eps'

plot [:255] [] 'kde_data.csv' using 1:2 notitle lw 3 lc rgb 'blue' with linespoints
