/*
Instituto Politécnico de Beja
Estruturas de Dados e Algoritmos

Autor: Pedro Caixinha
Data: 29/05/2017

Esta classe define objectos do tipo PerformanceTest que disponibilizam
a funcionalidade que foi utilizada para medir os tempos de execução das
várias funcionalidades desenvolvidas no âmbito deste projeto.
Os dados resultantes são exportados para um ficheiro em formato CSV, que contém
para cada imagem de teste a média dos tempos de execução, o desvião padrão e
o erro associado à média.
*/

import java.io.File;
import java.io.IOException;
import java.io.FileWriter;

public class PerformanceTest{

    static final int N_SAMPLE_IMAGES = 10;   // Número de imagens escaladas utilizadas nos testes
    static final int N_EXPS = 50;            // Número de medições realizadas em cada teste

    private int nThreads;                    // Número de fios de execução utilizados em cada teste

    /*
     * Método Construtor
     * Cria um objecto do tipo PerformanceTest.
     * @param nThreads Número de fios de execução utilizados em cada teste
     */
    public PerformanceTest(int nThreads){

        this.nThreads = nThreads;
    }

    /*
     * Método que testa o desempenho computacional do processo
     * de deteção de contornos pelo método de Canny.
     * @param printPrecision O número de casas decimais apresentadas nas funções "print"
     */
    public void cannyEdgeDetection(int printPrecision){

        double[][] executionTimeGrowth = new double[N_SAMPLE_IMAGES][4];   // Array bidimensional que contém as médias das medições obitidas para cada imagem processada
        double[] executionTimes = new double[N_EXPS];                      // Array que contém os tempos de execução de cada medição
        long startTime, finishTime;
        int nPixeis;
        double avg, stdDev, error;

        CannyEdgeDetector ced;
        System.out.println("Running performance tests for the Canny Edge Detection Algorithm.");

        for(int i = 1; i <= N_SAMPLE_IMAGES; i++){

            ced = new CannyEdgeDetector(("../test/images/@" + (i*10) + "%_Chambord_Castle_Northwest_facade_WITH_GAUSSIAN_BLUR.png"), this.nThreads);
            System.out.println("Testing with " + (i*10) + "% Scale Factor.");

            nPixeis = ced.getImageHeight() * ced.getImageWidth();
            executionTimeGrowth[i-1][0] = nPixeis;

            for(int j = 1; j <= N_EXPS; j++){

                System.out.println("Measurment: " + j + " of " + N_EXPS);

                startTime = System.nanoTime();
                ced.run();
                finishTime = System.nanoTime() - startTime;

                executionTimes[j-1] = finishTime * Math.pow(10, -9);
                System.out.println("Execution Time: " + String.format("%."+ printPrecision + "f", executionTimes[j-1]) + " s");
            }

            avg = this.average(executionTimes);
            stdDev = this.standardDeviation(executionTimes, avg);
            error = stdDev / Math.sqrt(N_EXPS);

            executionTimeGrowth[i-1][1] = avg;
            executionTimeGrowth[i-1][2] = stdDev;
            executionTimeGrowth[i-1][3] = error;

            System.out.println("Number of Measurements: " + N_EXPS);
            System.out.println("Execution Time Average: " + String.format("%."+ printPrecision + "f", avg) + "s");
            System.out.println("Standard Deviation: " + String.format("%."+ printPrecision + "f", stdDev) + "s");
            System.out.println("Measurment Error: " + String.format("%."+ printPrecision + "f", error) + "\n");
        }

        this.exportToCSV("canny_test.csv", executionTimeGrowth);
    }

    /*
     * Método que testa o desempenho computacional do processo de
     * extração da função densidade de probabilidades.
     * @param printPrecision O número de casas decimais apresentadas nas funções "print"
     */
    public void kde(int printPrecision){

        double[][] executionTimeGrowth = new double[N_SAMPLE_IMAGES][4];   // Array bidimensional que contém as médias das medições obitidas para cada imagem processada
        double[] executionTimes = new double[N_EXPS];                      // Array que contém os tempos de execução de cada medição
        long startTime, finishTime;
        int nPixeis;
        double avg, stdDev, error;

        KernelDensityEstimation kde;
        System.out.println("Running performance tests for the Kernel Density Estimation.");

        for(int i = 1; i <= N_SAMPLE_IMAGES; i++){

            kde = new KernelDensityEstimation(("../test/images/@" + (i*10) + "%_Chambord_Castle_Northwest_facade_WITH_GAUSSIAN_BLUR.png"), this.nThreads);
            System.out.println("Testing with " + (i*10) + "% Scale Factor.");

            nPixeis = kde.getImageHeight() * kde.getImageWidth();
            executionTimeGrowth[i-1][0] = nPixeis;

            for(int j = 1; j <= N_EXPS; j++){

                System.out.println("Measurment: " + j + " of " + N_EXPS);

                startTime = System.nanoTime();
                kde.run();
                finishTime = System.nanoTime() - startTime;

                executionTimes[j-1] = finishTime * Math.pow(10, -9);
                System.out.println("Execution Time: " + String.format("%."+ printPrecision + "f", executionTimes[j-1]) + " s");
            }

            avg = this.average(executionTimes);
            stdDev = this.standardDeviation(executionTimes, avg);
            error = stdDev / Math.sqrt(N_EXPS);

            executionTimeGrowth[i-1][1] = avg;
            executionTimeGrowth[i-1][2] = stdDev;
            executionTimeGrowth[i-1][3] = error;

            System.out.println("Number of Measurements: " + N_EXPS);
            System.out.println("Execution Time Average: " + String.format("%."+ printPrecision + "f", avg) + "s");
            System.out.println("Standard Deviation: " + String.format("%."+ printPrecision + "f", stdDev) + "s");
            System.out.println("Measurment Error: " + String.format("%."+ printPrecision + "f", error) + "\n");
        }

        this.exportToCSV("kde_test.csv", executionTimeGrowth);
    }

    /*
     * Método que testa o desempenho computacional do processo de
     * aplicação de desfoque Gaussiano.
     * @param printPrecision O número de casas decimais apresentadas nas funções "print"
     */
    public void gaussianBlur(int printPrecision){

        double[][] executionTimeGrowth = new double[N_SAMPLE_IMAGES][4];  // Array bidimensional que contém as médias das medições obitidas para cada imagem processada
        double[] executionTimes = new double[N_EXPS];                     // Array que contém os tempos de execução de cada medição
        long startTime, finishTime;

        double sigma = 0.84089642;
        GaussFilter gbf;
        int nPixeis;
        double avg, stdDev, error;

        System.out.println("Running performance tests for the Gaussian Blur algorithm.");

        for(int i = 1; i <= N_SAMPLE_IMAGES; i++){

            gbf = new GaussFilter(sigma, ("../test/images/@" + (i*10) + "%_Chambord_Castle_Northwest_facade_WITH_GAUSSIAN_NOISE.png"), this.nThreads);
            System.out.println("Testing with " + (i*10) + "% Scale Factor.");

            nPixeis = gbf.getImageHeight() * gbf.getImageWidth();
            executionTimeGrowth[i-1][0] = nPixeis;

            for(int j = 1; j <= N_EXPS; j++){

                System.out.println("Measurment: " + j + " of " + N_EXPS);

                startTime = System.nanoTime();
                gbf.run();
                finishTime = System.nanoTime() - startTime;

                executionTimes[j-1] = finishTime * Math.pow(10, -9);
                System.out.println("Execution Time: " + String.format("%."+ printPrecision + "f", executionTimes[j-1]) + " s");
            }

            avg = this.average(executionTimes);
            stdDev = this.standardDeviation(executionTimes, avg);
            error = stdDev / Math.sqrt(N_EXPS);

            executionTimeGrowth[i-1][1] = avg;
            executionTimeGrowth[i-1][2] = stdDev;
            executionTimeGrowth[i-1][3] = error;

            System.out.println("Number of Measurements: " + N_EXPS);
            System.out.println("Execution Time Average: " + String.format("%."+ printPrecision + "f", avg) + "s");
            System.out.println("Standard Deviation: " + String.format("%."+ printPrecision + "f", stdDev) + "s");
            System.out.println("Measurment Error: " + String.format("%."+ printPrecision + "f", error) + "\n");
        }

        this.exportToCSV("gaussianBlur_test.csv", executionTimeGrowth);
    }

    /*
     * Método que testa o desempenho computacional do processo de
     * conversção da imagem representada no espaço RGB para escala
     * de cinzentos.
     * @param printPrecision O número de casas decimais apresentadas nas funções "print"
     */
    public void grayscaleConversion(int printPrecision){

        double[][] executionTimeGrowth = new double[N_SAMPLE_IMAGES][4]; // Array bidimensional que contém as médias das medições obitidas para cada imagem processada
        double[] executionTimes = new double[N_EXPS];                    // Array que contém os tempos de execução de cada medição
        long startTime, finishTime;
        ConversaoRGB conversaoRGB;
        int nPixeis;
        double avg, stdDev, error;

        System.out.println("Running performance tests for RGB to Grayscale conversion algorithm.");

        for(int i = 1; i <= N_SAMPLE_IMAGES; i++){

            conversaoRGB = new ConversaoRGB(("../test/images/@" + (i*10) + "%_Chambord_Castle_Northwest_facade.jpg"), this.nThreads);
            System.out.println("Testing with " + (i*10) + "% Scale Factor.");

            nPixeis = conversaoRGB.getImageHeight() * conversaoRGB.getImageWidth();
            executionTimeGrowth[i-1][0] = nPixeis;

            for(int j = 1; j <= N_EXPS; j++){

                System.out.println("Measurment: " + j + " of " + N_EXPS);

                startTime = System.nanoTime();
                conversaoRGB.run();
                finishTime = System.nanoTime() - startTime;

                executionTimes[j-1] = finishTime * Math.pow(10, -9);
                System.out.println("Execution Time: " + String.format("%."+ printPrecision + "f", executionTimes[j-1]) + " s");
            }

            avg = this.average(executionTimes);
            stdDev = this.standardDeviation(executionTimes, avg);
            error = stdDev / Math.sqrt(N_EXPS);

            executionTimeGrowth[i-1][1] = avg;
            executionTimeGrowth[i-1][2] = stdDev;
            executionTimeGrowth[i-1][3] = error;

            System.out.println("Number of Measurements: " + N_EXPS);
            System.out.println("Execution Time Average: " + String.format("%."+ printPrecision + "f", avg) + "s");
            System.out.println("Standard Deviation: " + String.format("%."+ printPrecision + "f", stdDev) + "s");
            System.out.println("Measurment Error: " + String.format("%."+ printPrecision + "f", error) + "\n");
        }

        this.exportToCSV("grayscaleConversion_test.csv", executionTimeGrowth);
    }

    /*
     * Método que cálcula a média dos tempos de execução obtidos
     * em cada medição.
     * @param executionTimes Array que contém os tempos de execução de cada medição
     * @return A média dos tempos de execução
     */
    public double average(double[] executionTimes){

        double sum = 0.0;

        for(double xi : executionTimes) sum += xi;

        return sum / executionTimes.length;
    }

    /*
     * Método que cálcula a variância em relação média dos tempos
     * de execução obtidos em cada medição.
     * @param executionTimes Array que contém os tempos de execução de cada medição
     * @return A variância em relação à média dos tempos de execução de cada medição
     */
    public double standardDeviation(double[] executionTimes, double average){
        double sum = 0.0;

        for(double xi : executionTimes) sum += ((xi - average) * (xi - average));

        return sum / executionTimes.length;
    }

    /*
     * Método que permite exportar os dados reultantes de cada teste
     * para um ficheiro em formato CSV.
     * @param filename O nome do ficheiro
     * @param data Array bidimensional que contém as médias das medições obitidas para cada imagem processada
     */
    public void exportToCSV(String filename, double[][] data){

        System.out.println("Writing data to file " + filename);

        try {
            File exportFile = new File(filename);
            FileWriter fw = new FileWriter(exportFile);

            for(int i = 0; i < data.length; i++){
                fw.write(data[i][0] + "," + data[i][1] + "," + data[i][2] + "," + data[1][3] + "\n");
            }

            fw.flush();
            fw.close();
        }catch(IOException e) {
            System.out.println(e);
        }
    }
}
