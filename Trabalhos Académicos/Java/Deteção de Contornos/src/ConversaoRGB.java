/*
Instituto Politécnico de Beja
Estruturas de Dados e Algoritmos

Autor: Pedro Caixinha
Data: 29/05/2017

Esta classe define objectos do tipo ConversaoRGB que disponibilizam
funcionalidade que permite converter uma imagem representa no espaço
de cores RGB para escala de cinzas.
*/

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class ConversaoRGB {
    private BufferedImage image;     // Objecto que descreve a imagem através um buffer que permite aceder aos dados da mesma
    private int imageHeight;         // Altura da imagem
    private int imageWidth;          // Largura da imagem
    private int[][] intensities;     // Array bidimensional que armezena as intensidades de cada pixel da imagem
    private int[][][] LUT;           // Array tridimensional que representa uma tabela de consultas das codificações RGB
    private int nThreads;            // Número de fios de execução

    /*
     * Método Construtor
     * Cria um objecto do tipo ConversaoRGB.
     * Executa o método que constrói tabela de consultas.
     * @param filename Nome do ficheiro
     * @param nThreads Número de fios de execução
     */
    public ConversaoRGB(String filename, int nThreads){

        try {
            this.image = ImageIO.read(new File(filename));
            this.imageHeight = this.image.getHeight();
            this.imageWidth = this.image.getWidth();
            this.intensities = new int[this.imageHeight][this.imageWidth];
        }catch(IOException e) {
            System.out.println(e);
        }
        this.LUT = new int[256][256][256];
        this.nThreads = nThreads;

        this.buildLookUpTable();
    }

    /*
     * Método que constrói a LookUpTable.
     * Descodifica as componentes RBG da imagem e armazena
     * na tabela de consultas a associação das mesmas com
     * respectivo valor RGB codificado.
     */
    private void buildLookUpTable(){

        int rgb, r, g, b;

        for (int i = 0; i < this.imageHeight; i++) {
            for (int j = 0; j < this.imageWidth; j++) {
                rgb = this.image.getRGB(j, i);

                r = (rgb & 0xff0000) >> 16;
                g = (rgb & 0xff00) >> 8;
                b = rgb & 0xff;

                this.LUT[r][g][b] = rgb;
            }
        }
    }

    /*
     * Método que executa o processo de conversão da imagem
     * para escala de cinzentos.
     * Realiza a transformação linear do espaço CIE1931 por
     * meio da tabela de consultas, armazenado na mesma os valores
     * transformados.
     * Após esta operação armazena no array intensities as novas
     * intensidades em escala de cinzentos com recurso a multíplos
     * fios de execução.
     */
    public void run(){

        int r, g, b, yLinear;
        double rLinear, gLinear, bLinear;

        for(int i = 0; i < 256; i++){
           for(int j = 0; j < 256; j++){
              for(int k = 0; k < 256; k++){
                 if(this.LUT[i][j][k] != 0){
                    r = (this.LUT[i][j][k] >> 16) & 0xFF;
                    g = (this.LUT[i][j][k] >> 8 ) & 0xFF;
                    b = (this.LUT[i][j][k]) & 0xFF;

                    rLinear = ((32406 * r) + (-15372 * g) + (-4986 * b)) * Math.pow(10 ,-4);
                    gLinear = ((-9689 * r) + (18758 * g) + (415 * b)) * Math.pow(10, -4);
                    bLinear = ((557 * r) + (-2040 * g) + (10570 * b)) * Math.pow(10, -4);

                    yLinear = (int) (((2126 * rLinear) + (7152 * gLinear) + (722 * bLinear)) * Math.pow(10, -4));

                    this.LUT[i][j][k] = yLinear;
                 }
              }
           }
        }

        ConversaoRGBThread[] threads = new ConversaoRGBThread[this.nThreads];

        int step = this.imageWidth / this.nThreads;
        int startCol = 0;
        int endCol = 0;

        try{
            for(int i = 1; i <= this.nThreads; i++){
                startCol = (i == 1) ? 0 : endCol;
                endCol = (i == this.nThreads) ? this.imageWidth : step * i;

                threads[i-1] = new ConversaoRGBThread(this.image,
                                                      this.imageHeight,
                                                      this.intensities,
                                                      this.LUT,
                                                      startCol,
                                                      endCol);
            }

            for(ConversaoRGBThread t : threads) t.start();
            for(ConversaoRGBThread t : threads) t.join();
        }catch(InterruptedException e) {
            System.out.println(e);
        }
    }

    /*
     * Método que devolve o array bidimensional que contém
     * as intensidades da imagem na escala de cinzentos
     * @return O array com intensidades em escala de cinzentos
     */
    public int[][] getIntensities(){
        return this.intensities;
    }

    /*
     * Método que permite obter a altura da imagem
     * @return A altura da imagem
     */
    public int getImageHeight(){
        return this.imageHeight;
    }

    /*
     * Método que permite obter a largura da imagem
     * @return A largura da imagem
     */
    public int getImageWidth(){
        return this.imageWidth;
    }
}
