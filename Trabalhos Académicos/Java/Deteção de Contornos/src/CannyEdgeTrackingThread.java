/*
Instituto Politécnico de Beja
Estruturas de Dados e Algoritmos

Autor: Pedro Caixinha
Data: 07/06/2017

Esta classe define objectos do tipo CannyEdgeTrackingThread que
definem os fios de execução utilizados na fase de rastreamento
de máximos do método de Canny.
*/
import java.lang.Thread;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;

public class CannyEdgeTrackingThread extends Thread {

    private int[][] intensities;    // Array bidimensional que armezena as intensidades de cada pixel
    private double thSup;           // Limiar superior
    private double thInf;           // Limiar Inferior
    private int startCol;           // Indice da coluna de processamento inicial
    private int endCol;             // Indice da coluna de processamento final

    /*
     * Método Construtor
     * Cria um objecto do tipo CannyEdgeTrackingThread.
     * @param intensities Array bidimensional que armezena as intensidades de cada pixel da imagem
     * @param thSup Limiar superior
     * @param thInf Limiar inferior
     * @param startCol Indice da coluna de processamento inical
     * @param endCol Indice da coluna de processamento final
     */
    public CannyEdgeTrackingThread (int[][] intensities,
                                    double thSup,
                                    double thInf,
                                    int startCol,
                                    int endCol){

        this.intensities = intensities;
        this.thSup = thSup;
        this.thInf = thInf;
        this.startCol = startCol;
        this.endCol = endCol;
    }

    /*
     * Método de execução do fio.
     * Com base no limiar superior e inferior, este método suprime os pixeis que se encontram
     * abaixo do limiar inferior, define como máximos os pixeis cujas intensidades
     * são iguais ou superiores ao limiar superior, e analisa como base numa
     * conectividade de 8, os pixies que se encontram entre ambos os limiares. Caso
     * estes estejam conectados a um pixel com intensidade igual ou superior ao limiar superior,
     * são perservados, de outra forma são suprimidos.
     */
    @Override
    public void run(){

        for (int y = 1; y < this.intensities.length - 1; y++) {
            for (int x = this.startCol; x < this.endCol; x++) {
                if(this.intensities[y][x] >= thSup){
                    this.intensities[y][x] = 255;
                }else if(this.intensities[y][x] < thSup &&
                         this.intensities[y][x] >= thInf){
                    if(this.intensities[y][x-1] >= thSup ||
                       this.intensities[y-1][x-1] >= thSup ||
                       this.intensities[y-1][x] >= thSup ||
                        this.intensities[y-1][x+1] >= thSup){
                        this.intensities[y][x] = 255;
                    }else{
                        this.intensities[y][x] = 0;
                    }
                }else{
                    this.intensities[y][x] = 0;
                }
            }
        }
    }
}
