/*
Instituto Politécnico de Beja
Estruturas de Dados e Algoritmos

Autor: Pedro Caixinha
Data: 07/06/2017

Esta classe define objectos do tipo CannyNonMaximumSuppressionThread que
definem os fios de execução utilizados na fase de supressão de
não-máximos do método de detectção de contornos de Canny.
*/

import java.lang.Thread;

public class CannyNonMaximumSuppressionThread extends Thread {

    private int[][] intensities;          // Array bidimensional que armezena as intensidades de cada pixel da imagem final
    private float[][] gradientDirection;  // Array bidimensional que contém a direcção do gradiente de cada pixel da imagem opós a convolução com o operador de Sobel
    private int startCol;                 // Indice da coluna de processamento incial
    private int endCol;                   // Indice da coluna de processamento final

    /*
     * Método Construtor
     * Cria um objecto do tipo CannyNonMaximumSuppressionThread.
     * @param intensities Array bidimensional que armezena as intensidades de cada pixel da imagem final
     * @param gradientDirection Array bidimensional que contém a direcção do gradiente de cada pixel da imagem opós a convolução com o operador de Sobel
     * @param startCol Indice da coluna de processamento inical
     * @param endCol Indice da coluna de processamento final
     */
    public CannyNonMaximumSuppressionThread (int[][] intensities,
                                             float[][] gradientDirection,
                                             int startCol,
                                             int endCol){

        this.intensities = intensities;
        this.gradientDirection = gradientDirection;
        this.startCol = startCol;
        this.endCol = endCol;
    }

    /*
     * Método de execução do fio.
     * São percorridos todos os pixies da imagem e comparadas as intensidades dos
     * gradientes destes, com os pixies vizinhos na mesma direção do gradiente.
     * Caso algum a intensidade do gradiente de algum dos pixies vizinhos seja superior
     * ao pixel em análise, o mesmo é suprimido.
     */
    @Override
    public void run(){

        float gDirection;

        for (int y = 0; y < this.intensities.length; y++) {
            for (int x = this.startCol; x < this.endCol; x++) {
                gDirection = this.gradientDirection[y][x];

                if((gDirection >= 0 && gDirection <= 22.5) || (gDirection > 157.5 && gDirection <= 180)){

                    // ROUNDED GRADIENT ANGLE IS 0° -- COMPARES TO [y][x-1] and [y][x+1]

                    if(x-1 < 0){
                        if(this.intensities[y][x] < this.intensities[y][x+1]){
                            this.intensities[y][x] = 0;
                        }
                    }else if(x+1 >= this.intensities[0].length){
                        if(this.intensities[y][x] < this.intensities[y][x-1]){
                            this.intensities[y][x] = 0;
                        }
                    }else{
                        if(this.intensities[y][x] < this.intensities[y][x-1] || this.intensities[y][x] < this.intensities[y][x+1]){
                            this.intensities[y][x] = 0;
                        }
                    }
                }else if((gDirection > 22.5 && gDirection <= 67.5)){

                    // ROUNDED GRADIENT ANGLE IS 45° -- COMPARES TO [y-1][x+1] and [y+1][x-1]

                    if(y-1 < 0 || x+1 <= this.intensities[0].length){
                        if(y+1 < this.intensities.length && x-1 >= 0){
                            if(this.intensities[y][x] < this.intensities[y+1][x-1]){
                                this.intensities[y][x] = 0;
                            }
                        }
                    }else if(y+1 >= this.intensities.length || x-1 < 0){
                        if(y-1 >= 0 && x+1 < this.intensities[0].length){
                            if(this.intensities[y][x] < this.intensities[y-1][x+1]){
                                this.intensities[y][x] = 0;
                            }
                        }
                    }else{
                        if(this.intensities[y][x] < this.intensities[y-1][x+1] || this.intensities[y][x] < this.intensities[y+1][x-1]){
                            this.intensities[y][x] = 0;
                        }
                    }
                }else if((gDirection > 67.5 && gDirection <= 112.5)){

                    // ROUNDED GRADIENT ANGLE IS 90° -- COMPARES TO [y-1][x] and [y+1][x]

                    if(y-1 < 0){
                        if(this.intensities[y][x] < this.intensities[y+1][x]){
                            this.intensities[y][x] = 0;
                        }
                    }else if(y+1 >= this.intensities.length){
                        if(this.intensities[y][x] < this.intensities[y-1][x]){
                            this.intensities[y][x] = 0;
                        }
                    }else{
                        if(this.intensities[y][x] < this.intensities[y-1][x] || this.intensities[y][x] < this.intensities[y+1][x]){
                            this.intensities[y][x] = 0;
                        }
                    }
                }else {

                    // ROUNDED GRADIENT ANGLE IS 135° -- COMPARES TO [y-1][x-1] and [y+1][x+1]

                    if(y-1 < 0 || x-1 < 0){
                        if(y+1 < this.intensities.length && x+1 < this.intensities[0].length){
                            if(this.intensities[y][x] < this.intensities[y+1][x+1]){
                                this.intensities[y][x] = 0;
                            }
                        }
                    }else if(y+1 >= this.intensities.length || x+1 >= this.intensities[0].length){
                        if(y-1 >= 0 && x-1 >= 0){
                            if(this.intensities[y][x] < this.intensities[y-1][x-1]){
                                this.intensities[y][x] = 0;
                            }
                        }
                    }else {
                        if(this.intensities[y][x] < this.intensities[y-1][x-1] || this.intensities[y][x] < this.intensities[y+1][x+1]){
                            this.intensities[y][x] = 0;
                        }
                    }
                }
            }
        }
    }
}
