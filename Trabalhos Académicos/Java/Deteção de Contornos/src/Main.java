/*
Instituto Politécnico de Beja
Estruturas de Dados e Algoritmos

Autor: Pedro Caixinha
Data: 29/05/2017

Este programa contém funcionalidade que permite realizar as seguintes
operações a uma imagem.

- Conversão do sistema de cores RGB para escala de cinzas;
- Contaminação da imagem com ruído gaussiano;
- Aplicação de um filtro de desfoque gaussiano;
- Deteção de contornos com recurso ao método de Canny.
*/

import java.awt.image.BufferedImage;
import java.awt.image.Raster;
import java.util.Random;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.stream.*;
import java.awt.image.WritableRaster;
import java.util.Arrays;
import java.util.IntSummaryStatistics;

public class Main {

    // NÚMERO DE FIOS DE EXECUÇÃO
    private static final int NTHREADS = 2;

    // CAMINHO DE FICHEIROS
    private static final String ORIGINAL_IMAGE = "Chambord_Castle_Northwest_facade.jpg";                           // FICHEIRO DA IMAGEM ORIGINAL
    private static final String GRAYSCALE_IMAGE = "Chambord_Castle_Northwest_facade_GRAYSCALE.png";                // FICHEIRO DA IMAGEM EM ESCALA DE CINZAS
    private static final String GAUSSIAN_NOISE_IMAGE = "Chambord_Castle_Northwest_facade_WITH_GAUSSIAN_NOISE.png"; // FICHEIRO DA IMAGEM CONTAMINADA COM RUÍDO GAUSSIANO
    private static final String GAUSSIAN_BLUR_IMAGE = "Chambord_Castle_Northwest_facade_WITH_GAUSSIAN_BLUR.png";   // FICHEIRO DA IMAGEM COM DESFOQUE GAUSSIANO
    private static final String EDGE_DETECTION_IMAGE = "Chambord_Castle_Northwest_facade_EDGES.png";               // FICHEIRO DA IMAGEM COM DETEÇÃO DE CONTORNOS

    /*
     * Método Main
     *
     * Permite executar as funcionalidades implementadas.
     * Não são necessários argumentos.
     */
    public static void main(String[] args) {

        // CONVERSÃO PARA ESCALA DE CINZAS
        //grayscaleConversion();

        // CONTAMINAÇÃO COM RUÍDO GAUSSIANO
        //addGaussianNoise();

        // APLICAÇÃO DO FILTRO DE DESFOQUE GAUSSIANO
        //gaussianBlurFilter();

        // FUNÇÃO DENSIDADE DE PROBABILIDADES (P.D.F.)
        kernelDensityEstimation();

        // DETEÇÃO DE CONTORNOS
        //cannyEdgeDetection();

        // TESTES DE DESEMPENHO
        //PerformanceTest test = new PerformanceTest(NTHREADS);
        //test.grayscaleConversion(3);
        //test.gaussianBlur(3);
        //test.kde(7);
        //test.cannyEdgeDetection(3);
    }

    /*
     * Realiza a conversão da imagem original para escala de cinzas
     * e escreve o resultado para um novo ficheiro.
     */
    private static void grayscaleConversion(){

        // CONVERSÃO PARA ESCALA DE CINZAS

        ConversaoRGB conversaoRGB = new ConversaoRGB(ORIGINAL_IMAGE, NTHREADS);
        conversaoRGB.run();

        writeImage(conversaoRGB.getIntensities(), GRAYSCALE_IMAGE);
        System.out.println("Done.");
    }

    /*
     * Contamina a imagem em escala de cinzas com ruído Gaussiano. É utilizado o valor de
     * σ = 30 e a função nextGaussian disponivel na biblioteca padrão de Java, que
     * permite a geração pseudo aletória de números com distribuição Gaussiana.
     * O resultado da operação é normalizado para o intervalo de intensidades [0..255] e
     * escrito para um novo ficheiro.
     */
    private static void addGaussianNoise(){

        // CONTAMINAÇÃO COM RUÍDO GAUSSIANO

        BufferedImage image = null;
        int imageHeight = 0;
        int imageWidth = 0;
        int[][] intensities = null;
        int sigma = 30;

        try {
            image = ImageIO.read(new File(GRAYSCALE_IMAGE));
            imageHeight = image.getHeight();
            imageWidth = image.getWidth();
            intensities = new int[imageHeight][imageWidth];
        }catch(IOException e) {
            System.out.println(e);
        }

        Raster raster = image.getData();
        Random rand = new Random();
        int y;

        for (int i = 0; i < imageHeight; i++) {
            for (int j = 0; j < imageWidth; j++) {
                y = raster.getSample(j, i, 0);
                intensities[i][j] = (int) ((rand.nextGaussian() * sigma) + y);
            }
         }

        // NORMALIZAÇÃO

        normalizeImage(intensities);

        writeImage(intensities, GAUSSIAN_NOISE_IMAGE);
        System.out.println("Done.");
    }

    /*
     * Aplica um filtro de desfoque Gaussiano a uma imagem e
     * escreve o resultado para um novo ficheiro.
     */
    private static void gaussianBlurFilter(){

        // APLICAÇÃO DO FILTRO DE DESFOQUE GAUSSIANO

        double sigma = 0.84089642;
        //double sigma = 1.0;

        GaussFilter gbf = new GaussFilter(sigma, GAUSSIAN_NOISE_IMAGE, NTHREADS);
        gbf.run();

        writeImage(gbf.getIntensities(), GAUSSIAN_BLUR_IMAGE);
        System.out.println("Done.");
    }

    /*
     * Cálcula a função densidade de probabilidade da imagem onde foi
     * previamente a aplicado o filtro de desfoque Gaussiano, com recurso
     * ao método conhecido por Kernel Density Estimation.
     * Os extraídos da função são escritos para um ficheiro em formato CSV.
     */
    private static void kernelDensityEstimation(){

        // KERNEL DENSITY ESTIMATION

        KernelDensityEstimation kde = new KernelDensityEstimation(GAUSSIAN_BLUR_IMAGE, NTHREADS);
        kde.run();

        kde.exportKDEData();
        System.out.println("Done.");
    }

    /*
     * Executa o processo de deteção de contornos com recurso ao método
     * de Canny na imagem previamente processada com o filtro de desfoque
     * Gaussiano.
     * O resultado é escrito para um novo ficheiro.
     */
    private static void cannyEdgeDetection(){

        // CANNY - DETEÇÃO DE CONTORNOS

        CannyEdgeDetector ced = new CannyEdgeDetector(GAUSSIAN_BLUR_IMAGE, NTHREADS);
        ced.run();

        writeImage(ced.getIntensities(), EDGE_DETECTION_IMAGE);
        System.out.println("Done.");
     }

    /*
     * Executa o processo de normalização de uma imagem em escala
     * de cinzas.
     * @param intensities Intensidades da imagem
     */
    public static void normalizeImage(int[][] intensities){

        IntSummaryStatistics stats = Arrays.stream(intensities).flatMapToInt(Arrays::stream).summaryStatistics();
        int min = stats.getMin();
        int max = stats.getMax();

        for (int i = 0; i < intensities.length; i++) {
            for (int j = 0; j < intensities[0].length; j++) {
                intensities[i][j] = (int) (255 * ((double) (intensities[i][j] - min) / (max - min)));
            }
        }
    }

    /*
     * Escreve um novo ficheiro de imagem.
     * @param intensities Intensidades da imagem
     * @param filename Nome do ficheiro
     */
    public static void writeImage(int[][] intensities, String filename){

        System.out.println("Writing image to file " + filename);

        try {
            BufferedImage image = new BufferedImage(intensities[0].length,
                                                    intensities.length,
                                                    BufferedImage.TYPE_BYTE_GRAY);

            int[] flat = Arrays.stream(intensities).flatMapToInt(Arrays::stream).toArray();

            WritableRaster raster = image.getRaster();
            raster.setSamples(0, 0, image.getWidth(), image.getHeight(), 0, flat);

            File imageFile = new File(filename);
            ImageIO.write(image, "PNG", imageFile);
        }catch(IOException e) {
            System.out.println(e);
        }
    }
}
