/*
Instituto Politécnico de Beja
Estruturas de Dados e Algoritmos

Autor: Pedro Caixinha
Data: 07/06/2017

Esta classe define objectos do tipo CannyEdgeDetector que disponibilizam
funcionalidade que permite deter os contornos numa imagem, com base
no método de Canny.
O processo de convolução toma partido da propriedade separável do operador
de Sobel no sentido de reduzir tempo de computação gasto na aplicação do
mesmo.
A imagem resultante terá as dimensões:
- Altura da imagem original - 2 * (dimensão do filtro - 2)
- Largura da imagem orginal - 2 * (dimensão do filtro - 2)
*/

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.image.Raster;
import java.util.Arrays;
import java.util.IntSummaryStatistics;

public class CannyEdgeDetector {
    private static final int FILTER_SIZE = 3;                   // Dimensão do filtro
    private static final int FILTER_OFFSET = FILTER_SIZE / 2;   // Dimensão do desfasamento
    static final float TH_SUP_PERCENTAGE = 0.275f;              // Percentagem do limiar Superior
    static final float TH_INF_PERCENTAGE = 0.2f;                // Percentagem do limiar Inferior

    private BufferedImage image;             // Objecto que descreve a imagem através um buffer que permite aceder aos dados da mesma
    private int imageHeight;                 // Altura da imagem
    private int imageWidth;                  // Largura da imagem
    private Raster raster;                   // Objecto que representa um array rectangular de pixeis
    private int[][] tempGX;                  // Array bidimensional contendo os resultados da derivada GX resultante da aplicação do operador de Sobel
    private int[][] tempGY;                  // Array bidimensional contendo os resultados da derivada GY resultante da aplicação do operador de Sobel
    private int[][] intensities;             // Array bidimensional que armezena as intensidades de cada pixel da imagem final
    private float[][] gradientDirection;     // Array bidimensional que armazena a direcção do gradiente de cada pixel da imagem opós a convolução com o operador de Sobel
    private int nThreads;                    // Número de fios de execução


    /*
     * Método Construtor
     * Cria um objecto do tipo CannyEdgeDetector.
     * @param filename Nome do ficheiro
     * @param nThreads Número de fios de execução
     */
    public CannyEdgeDetector(String filename, int nThreads){

        try {
            this.image = ImageIO.read(new File(filename));
            this.imageHeight = this.image.getHeight();
            this.imageWidth = this.image.getWidth();
            this.raster = this.image.getData();
            this.tempGX = new int[this.imageHeight - (2*FILTER_OFFSET)][this.imageWidth - (2*FILTER_OFFSET)];
            this.tempGY = new int[this.imageHeight - (2*FILTER_OFFSET)][this.imageWidth - (2*FILTER_OFFSET)];
            this.intensities = new int[this.imageHeight - (4*FILTER_OFFSET)][this.imageWidth - (4*FILTER_OFFSET)];
            this.gradientDirection = new float[this.imageHeight - (4*FILTER_OFFSET)][this.imageWidth - (4*FILTER_OFFSET)];
        }catch(IOException e) {
            System.out.println(e);
        }
        this.nThreads = nThreads;
    }

    /*
     * Método que executa as várias fases do método de Canny,
     * relativamente ao processo deteção de contornos.
     */
    public void run(){

        // APLICAÇÃO DO OPERADOR DE SOBEL
        this.obtainGradients();

        //SUPRESSÃO DE NÃO-MÁXIMOS
        this.nonMaximumSuppression();

        // DETEÇÃO DE MÁXIMOS POR HISTERESE
        this.edgeTracking();
    }

    /*
     * Método que executa o processo de edge tracking do método de Canny,
     * com recurso a múltiplos fios de execução.
     *b
     */
    private void edgeTracking(){

        IntSummaryStatistics stats = Arrays.stream(this.intensities).flatMapToInt(Arrays::stream).summaryStatistics();
        int maxGrad = stats.getMax();

        float thSup = maxGrad * TH_SUP_PERCENTAGE;
        float thInf = thSup * TH_INF_PERCENTAGE;

        //System.out.println(maxGrad + " " + thSup + " " + thInf);

        CannyEdgeTrackingThread[] threads = new CannyEdgeTrackingThread[this.nThreads];

        int step = this.intensities[0].length / this.nThreads;
        int startCol = 0;
        int endCol = 0;

        for(int i = 1; i <= this.nThreads; i++){

            startCol = (i == 1) ? 1 : endCol;
            endCol = (i == this.nThreads) ? this.intensities[0].length - 1 : step * i;

            threads[i-1] = new CannyEdgeTrackingThread(this.intensities,
                                                       thSup,
                                                       thInf,
                                                       startCol,
                                                       endCol);
        }

        try {
            for(CannyEdgeTrackingThread t : threads) t.start();
            for(CannyEdgeTrackingThread t : threads) t.join();
        }catch(InterruptedException e) {
            System.out.println(e);
        }
    }

    /*
     * Método que executa a fase de supressão de não-máximos do
     * método de Canny, com recurso a múltiplos fios de execução.
     */
    private void nonMaximumSuppression(){

        CannyNonMaximumSuppressionThread[] threads = new CannyNonMaximumSuppressionThread[this.nThreads];

        int step = this.intensities[0].length / this.nThreads;
        int startCol = 0;
        int endCol = 0;

        for(int i = 1; i <= this.nThreads; i++){

            startCol = (i == 1) ? 0 : endCol;
            endCol = (i == this.nThreads) ? this.intensities[0].length : step * i;

            threads[i-1] = new CannyNonMaximumSuppressionThread(this.intensities,
                                                                this.gradientDirection,
                                                                startCol,
                                                                endCol);
        }

        try {
            for(CannyNonMaximumSuppressionThread t : threads) t.start();
            for(CannyNonMaximumSuppressionThread t : threads) t.join();
        }catch(InterruptedException e) {
            System.out.println(e);
        }
    }

    /*
     * Método que executa a fase de deteção dos gradientes e do ângulo da
     * da direcção do mesmos com recurso ao operador de Sobel.
     * Este processo é realizado com recurso a múltiplos fios de execução,
     * e com base na proriedade separável do operador de Sobel.
     */
    private void obtainGradients(){

        CannyGradientThread[] threads = new CannyGradientThread[this.nThreads];

        int stepFisrtPass = (this.imageWidth - (2*FILTER_OFFSET)) / this.nThreads;
        int stepSecondPass = (this.imageWidth - (4*FILTER_OFFSET)) / this.nThreads;

        int startColFirstPass = 0;
        int endColFirstPass = 0;
        int endRowFirstPass = this.imageHeight - FILTER_OFFSET;

        int startColSecondPass = 0;
        int endColSecondPass = 0;
        int endRowSecondPass = this.imageHeight - (3*FILTER_OFFSET);

        for(int i = 1; i <= this.nThreads; i++){

            startColFirstPass = (i == 1) ? FILTER_OFFSET : endColFirstPass;
            endColFirstPass = (i == this.nThreads) ? this.imageWidth - FILTER_OFFSET : stepFisrtPass * i;

            startColSecondPass = (i == 1) ? FILTER_OFFSET : endColSecondPass;
            endColSecondPass = (i == this.nThreads) ? this.imageWidth - (3*FILTER_OFFSET): stepSecondPass * i;

            threads[i-1] = new CannyGradientThread(this.image,
                                                   FILTER_OFFSET,
                                                   this.tempGX,
                                                   this.tempGY,
                                                   this.intensities,
                                                   this.gradientDirection,
                                                   startColFirstPass,
                                                   endColFirstPass,
                                                   endRowFirstPass,
                                                   startColSecondPass,
                                                   endColSecondPass,
                                                   endRowSecondPass
                                                   );

        }

        try {
            for(CannyGradientThread t : threads) t.start();
            for(CannyGradientThread t : threads) t.join();
        }catch(InterruptedException e) {
            System.out.println(e);
        }
    }

    /*
     * Método que permite obter a estrutura de dados que armazena as intensidades
     * filtradas de cada pixel.
     * @return Array bidimensional que contém as intensidades de cada pixel
     */
    public int[][] getIntensities(){
        return this.intensities;
    }

    /*
     * Método que permite obter a altura da nova imagem
     * @return A altura da nova imagem
     */
    public int getImageHeight(){
        return this.intensities.length;
    }

    /*
     * Método que permite obter a largura da nova imagem
     * @return A largura da nova imagem
     */
    public int getImageWidth(){
        return this.intensities[0].length;
    }
}
