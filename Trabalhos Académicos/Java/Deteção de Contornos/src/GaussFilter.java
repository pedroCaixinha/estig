/*
Instituto Politécnico de Beja
Estruturas de Dados e Algoritmos

Autor: Pedro Caixinha
Data: 29/05/2017

Esta classe define objectos do tipo GaussFilter que disponibilizam
funcionalidade que permite aplicar um filtro de desfoque Gaussiano
a uma imagem.
O processo de convolução toma partido da propriedade separável do filtro
Gaussiano no sentido de reduzir tempo de computação gasto na aplicação do
mesmo.
A imagem resultante terá as dimensões:
- Altura da imagem original - 2 * (dimensão do filtro - 2)
- Largura da imagem orginal - 2 * (dimensão do filtro - 2)
*/

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.image.BufferedImage;

public class GaussFilter {
    private static final int FILTER_SIZE = 7;                    // Dimensão do filtro
    private static final int FILTER_OFFSET = FILTER_SIZE / 2;    // Dimensão do desfasamento

    private BufferedImage image;                                 // Objecto que descreve a imagem através um buffer que permite aceder aos dados da mesma
    private int imageHeight;                                     // Altura da imagem
    private int imageWidth;                                      // Largura da imagem
    private double sigma;                                        // Valor do desvio padrão a utilizar no filtro
    private double[] filter;                                     // Array que irá conter o filtro
    private float[][] temp;                                      // Array bidimensional que ormazena as intentidades filtradas na primeira passagem
    private int[][] intensities;                                 // Array bidimensional que armezena as intensidades de cada pixel da imagem final
    private int nThreads;                                        // Número de fios de execução

    /*
     * Método Construtor
     * Cria um objecto do tipo GaussFilter.
     * Executa o método que constrói o filtro.
     * @param sigma Desvião a utilizar no filtro
     * @param filename Nome do ficheiro
     * @param nThreads Número de fios de execução
     */
    public GaussFilter(double sigma, String filename, int nThreads){

        try {
            this.image = ImageIO.read(new File(filename));
            this.imageHeight = this.image.getHeight();
            this.imageWidth = this.image.getWidth();
        }catch(IOException e) {
            System.out.println(e);
        }

        this.sigma = sigma;
        this.temp = new float[this.imageHeight - (2*FILTER_OFFSET)][this.imageWidth - (2*FILTER_OFFSET)];
        this.intensities = new int[this.imageHeight - (4*FILTER_OFFSET)][this.imageWidth - (4*FILTER_OFFSET)];

        //BUILD 1D FILTER
        this.filter = new double[FILTER_SIZE];

        for(int i = -FILTER_SIZE/2; i <= FILTER_SIZE/2; i++){
            this.filter[i + (FILTER_SIZE / 2)] = gaussFunction(i);
        }

        this.nThreads = nThreads;
    }

    /*
     * Método que implementa a funcção de Gauss para 1 dimensão.
     * Devolve os valores que irão formar o filtro separável.
     * @param x Valor de x
     * @return A imagem de x
     */
    private double gaussFunction(int x){

        return (1 / (this.sigma * Math.sqrt(2 * Math.PI))) *
            Math.exp(-((x * x) / (2 * (this.sigma * this.sigma))));
    }

    /*
     * Método que executa o processo de aplicação do filtro com
     * recurso a multiplos fios de execução que irão executar cada
     * um as duas passagens do processo de convulação separável com
     * base nos limites previamente calculados relativamente á horizontal.
     */
    public void run() {

        GaussFilterThread[] threads = new GaussFilterThread[this.nThreads];

        int stepFisrtPass = (this.imageWidth - (2*FILTER_OFFSET)) / this.nThreads;
        int stepSecondPass = (this.imageWidth - (4*FILTER_OFFSET)) / this.nThreads;

        int startColFirstPass = 0;
        int endColFirstPass = 0;
        int endRowFirstPass = this.imageHeight - FILTER_OFFSET;

        int startColSecondPass = 0;
        int endColSecondPass = 0;
        int endRowSecondPass = this.imageHeight - (3*FILTER_OFFSET);

        for(int i = 1; i <= this.nThreads; i++){

            startColFirstPass = (i == 1) ? FILTER_OFFSET : endColFirstPass;
            endColFirstPass = (i == this.nThreads) ? this.imageWidth - FILTER_OFFSET : stepFisrtPass * i;

            startColSecondPass = (i == 1) ? FILTER_OFFSET : endColSecondPass;
            endColSecondPass = (i == this.nThreads) ? this.imageWidth - (3*FILTER_OFFSET): stepSecondPass * i;

            threads[i-1] = new GaussFilterThread(this.image,
                                                 this.filter,
                                                 FILTER_OFFSET,
                                                 this.temp,
                                                 this.intensities,
                                                 startColFirstPass,
                                                 endColFirstPass,
                                                 endRowFirstPass,
                                                 startColSecondPass,
                                                 endColSecondPass,
                                                 endRowSecondPass);
        }

        try {
            for(GaussFilterThread t : threads) t.start();
            for(GaussFilterThread t : threads) t.join();
        }catch(InterruptedException e) {
            System.out.println(e);
        }
    }

    /*
     * Método que permite a estrutura de dados que armazena as intensidades
     * filtradas de cada pixel.
     * @return Array bidimensional que contém as intensidades de cada pixel
     */
    public int[][] getIntensities(){
        return this.intensities;
    }

    /*
     * Método que permite obter a altura da nova imagem
     * @return A altura da nova imagem
     */
    public int getImageHeight(){
        return this.intensities.length;
    }

    /*
     * Método que permite obter a largura da nova imagem
     * @return A largura da nova imagem
     */
    public int getImageWidth(){
        return this.intensities[0].length;
    }
}
