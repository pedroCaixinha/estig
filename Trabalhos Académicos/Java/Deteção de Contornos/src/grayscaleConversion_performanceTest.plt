# Instituto Politécnico de Beja
# Estruturas de Dados e Algoritmos

# Autor: Pedro Caixinha
# Data: 07/06/2017

# Instruções que permitem ao programa gnuplot gerar para um terminal "postscript eps enhanced color"
# um gráfico representativo dos dados constantes do ficheiro "grayscaleConversion_test.csv"
# Este ficheiro deverá conter os dados resultantes da execução dos testes de medição do desempenho
# computacional da funcionalidade de conversão da imagem no espaço RGB para escala de cinzentos.
# O output é direcionado para o ficheiro com o nome "grayscaleConversion_GrowthRate.eps"

set datafile separator ','
set grid

set title "RGB to Grayscale Conversion Execution Times"
set xlabel "Number of Pixeis"
set ylabel "Execution Time (s)"

set term postscript eps enhanced color "Times-Roman,16"
set output 'grayscaleConversion_GrowthRate.eps'

plot 'grayscaleConversion_test.csv' using 1:2 notitle lw 3 lc rgb 'blue' with lines