/*
Instituto Politécnico de Beja
Estruturas de Dados e Algoritmos

Autor: Pedro Caixinha
Data: 29/05/2017

Esta classe define objectos do tipo KernelDensityEstimation que
disponibilizam funcionalidade que permite extrair a função
densidade de probabilidade para as intensidades da imagem.
Este processo é feito com base num histograma das intensidades,
no sentido reduzir o numero operações a executar.
Permite também exportar os dados resultantes da função
pora um ficheiro em formato CSV.
*/

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.FileWriter;
import java.awt.image.Raster;

public class KernelDensityEstimation {

    private static final int GRAY_LEVELS = 256;                     // Número de intensidades disponiveis numa escala de cinzentos
    private static final String OUTPUT_DATA_FILE = "kde_data.csv";  // Nome do ficheiro CSV para o qual serão exportados os dados

    private BufferedImage image;  // Objecto que descreve a imagem através um buffer que permite aceder aos dados da mesma
    private int imageHeight;      // Altura da imagem
    private int imageWidth;       // Largura da imagem
    private int nPixeis;          // Número de pixies da imagem
    private Raster raster;        // Objecto que representa um array rectangular de pixeis
    private int[] histogram;      // Array que representa o histograma de intensidades
    private float[] kde;          // Array que irá armazenar as imagens resultantes da função
    private int nThreads;         // Número de fios de execução

    /*
     * Método Construtor
     * Cria um objecto do tipo KernelDensityEstimation.
     * @param filename Nome do ficheiro
     * @param nThreads Número de fios de execução
     */
    public KernelDensityEstimation(String filename, int nThreads){

        try {
            this.image = ImageIO.read(new File(filename));
            this.imageHeight = this.image.getHeight();
            this.imageWidth = this.image.getWidth();
            this.nPixeis = this.imageHeight * this.imageWidth;
            this.raster = this.image.getData();
        }catch(IOException e) {
            System.out.println(e);
        }

        this.histogram = new int[GRAY_LEVELS];
        this.kde = new float[GRAY_LEVELS];
        this.nThreads = nThreads;
    }

    /*
     * Método de execução.
     * Constrói o histograma de intensidades.
     * Cácula constantes necessárias à implemetação da função.
     * Executa o processamento da função recorrendo a multiplos fios
     * de execução.
     */
    public void run(){

        // CONTRUÇÃO DO HISTOGRAMA
        this.buildHistogram();

        // CÁLCULO DA VARIÂNCIA DAS INTENSIDADES
        double sigma = this.variance();

        // CÁLCULO DO ESTIMADOR DE LARGURA DE BANDA
        double h = 1.06 * sigma * Math.pow(this.nPixeis, -0.2);

        // CÁLCULO DA CONSTANTE K1
        double k1 = 1 / (this.nPixeis * h * Math.sqrt(2 * Math.PI));

        // CÁLCULO DA CONSTANTE K2
        double k2 = Math.exp(-0.5/(h * h));

        // EXECUÇÃO
        KernelDensityEstimationThread[] threads = new KernelDensityEstimationThread[this.nThreads];

        int step = GRAY_LEVELS / this.nThreads;
        int start = 0;
        int end = 0;

        try{
            for(int i = 1; i <= this.nThreads; i++){
                start = (i == 1) ? 0 : end;
                end = (i == this.nThreads) ? GRAY_LEVELS : step * i;

                threads[i-1] = new KernelDensityEstimationThread(this.kde,
                                             this.histogram,
                                             k1,
                                             k2,
                                             start,
                                             end);
            }

            for(KernelDensityEstimationThread t : threads) t.start();
            for(KernelDensityEstimationThread t : threads) t.join();
        }catch(InterruptedException e) {
            System.out.println(e);
        }
    }

    /*
     * Método que constrói o histograma de intensidades
     * utilizado no cálculo da função.
     */
    private void buildHistogram(){

        for(int y = 0; y < this.imageHeight; y++){
            for(int x = 0; x < this.imageWidth; x++){
                this.histogram[this.raster.getSample(x, y, 0)]++;
            }
        }
    }

    /*
     * Método que cálcula a variância recorrendo ao histograma de
     * intensidades.
     * @return A variância das intensidades da imagem em relação à média
     */
    private double variance(){
        double sum, avg;

        // CÁLCULO DA MÉDIA
        sum = 0.0;
        for(int i = 0; i < GRAY_LEVELS; i++){
            sum += i * this.histogram[i];
        }
        avg = sum / this.nPixeis;

        // CÁLCULO DA VARIÂNCIA
        sum = 0.0;
        for(int i = 0; i < GRAY_LEVELS; i++){
            sum += ((i - avg) * (i - avg)) * this.histogram[i];
        }

        return Math.sqrt(sum / this.nPixeis);
    }

    /*
     * Método que permite exportar os dados resultantes da função para
     * um ficheiro em formato CSV.
     */
    public void exportKDEData(){

        System.out.println("Writing data to file " + OUTPUT_DATA_FILE);

        try {
            File exportFile = new File(OUTPUT_DATA_FILE);
            FileWriter fw = new FileWriter(exportFile);

            for(int i = 0; i < GRAY_LEVELS; i++){
                fw.write(i + "," + this.kde[i] + "\n");
            }

            fw.flush();
            fw.close();
        }catch(IOException e) {
            System.out.println(e);
        }
    }

    /*
     * Permite obter a altura da imagem
     * @return A altura da imagem
     */
    public int getImageHeight(){
        return this.imageHeight;
    }

    /*
     * Permite obter a largura da imagem
     * @return A largura da imagem
     */
    public int getImageWidth(){
        return this.imageWidth;
    }
}
