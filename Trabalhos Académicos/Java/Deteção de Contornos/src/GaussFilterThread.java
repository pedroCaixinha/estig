/*
Instituto Politécnico de Beja
Estruturas de Dados e Algoritmos

Autor: Pedro Caixinha
Data: 29/05/2017

Esta classe define objectos do tipo GaussFilterThread que
definem os fios de execução utilizados no processo de aplicação
do filtro de desfoque Gaussiano.
*/

import java.lang.Thread;
import java.awt.image.BufferedImage;
import java.awt.image.Raster;

public class GaussFilterThread extends Thread {

    private BufferedImage image;     // Objecto que descreve a imagem através um buffer que permite aceder aos dados da mesma
    private Raster raster;           // Objecto que representa um array rectangular de pixeis
    private double[] filter;         // Array que irá conter o filtro
    private int filterOffset;        // Dimensão do desfasamento
    private float[][] temp;          // Array bidimensional que ormazena as intentidades filtradas na primeira passagem
    private int[][] intensities;     // Array bidimensional que armezena as intensidades de cada pixel da imagem final
    private int startColFirstPass;   // Indice da coluna de processamento inical para a primeira passagem
    private int endColFirstPass;     // Indice da coluna de processamento final para a primeira passagem
    private int endRowFirstPass;     // Indica da linha de processamento final para a primeira passagem
    private int startColSecondPass;  // Indice da coluna de processamento inical para a segunda passagem
    private int endColSecondPass;    // Indice da coluna de processamento final para a segunda passagem
    private int endRowSecondPass;    // Indica da linha de processamento final para a segunda passagem

    /*
     * Método Construtor
     * Cria um objecto do tipo GaussFilterThread.
     * @param filter Array que irá conter o filtro
     * @param filterOffset Dimensão do desfasamento
     * @param temp Array bidimensional que ormazena as intentidades filtradas na primeira passagem
     * @param intensities Array bidimensional que armezena as intensidades de cada pixel da imagem final
     * @param startColFirstPass Indice da coluna de processamento inical para a primeira passagem
     * @param endColFirstPass Indice da coluna de processamento final para a primeira passagem
     * @param endRowFirstPass Indica da linha de processamento final para a primeira passagem
     * @param startColSecondPass Indice da coluna de processamento inical para a segunda passagem
     * @param endColSecondPass Indice da coluna de processamento final para a segunda passagem
     * @param endRowSecondPass Indica da linha de processamento final para a segunda passagem
     */
    public GaussFilterThread(BufferedImage image,
                             double[] filter,
                             int filterOffset,
                             float[][] temp,
                             int[][] intensities,
                             int startColFirstPass,
                             int endColFirstPass,
                             int endRowFirstPass,
                             int startColSecondPass,
                             int endColSecondPass,
                             int endRowSecondPass){

        this.image = image;
        this.raster = this.image.getData();
        this.filter = filter;
        this.filterOffset = filterOffset;
        this.temp = temp;
        this.intensities = intensities;
        this.startColFirstPass = startColFirstPass;
        this.endColFirstPass = endColFirstPass;
        this.endRowFirstPass = endRowFirstPass;
        this.startColSecondPass = startColSecondPass;
        this.endColSecondPass = endColSecondPass;
        this.endRowSecondPass = endRowSecondPass;
    }

    /*
     * Método de execução do fio.
     * Percorre primeiramente a imagem original linha a linha e efectua a convolução
     * do filtro relativamente a cada pixel, guardando o resultado no array temp. De
     * seguida, percorre de coluna a coluna, as intensidades previamente fitradas do
     * array temp e torna a efectuar a convolução com o mesmo filtro guardando o resultado
     * final no array intensities.
     * Cada fio processa uma parte das estruturas de dados na horizontal, com base
     * nos limites estabelecidos.
     */
    @Override
    public void run(){

        float sum;

        for (int y = this.filterOffset; y < this.endRowFirstPass; y++) {
            for (int x = this.startColFirstPass; x < this.endColFirstPass; x++) {

                // ALONG Y

                sum = 0.0f;

                for(int i = -this.filterOffset; i <= this.filterOffset; i++){
                    sum += this.filter[i+this.filterOffset] * this.raster.getSample(x, y-i, 0);
                }

                this.temp[y-this.filterOffset][x-this.filterOffset] = sum;
            }
        }


        for (int y = this.filterOffset; y < this.endRowSecondPass; y++) {
            for (int x = this.startColSecondPass; x < this.endColSecondPass; x++) {

                // ALONG X

                sum = 0.0f;

                for(int i = -this.filterOffset; i <= this.filterOffset; i++){
                    sum += this.filter[i+this.filterOffset] * this.temp[y][x-i];
                }

                this.intensities[y-this.filterOffset][x-this.filterOffset] = (int) sum;
            }
        }

    }
}
