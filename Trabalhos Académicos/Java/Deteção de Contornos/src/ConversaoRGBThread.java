/*
Instituto Politécnico de Beja
Estruturas de Dados e Algoritmos

Autor: Pedro Caixinha
Data: 29/05/2017

Esta classe define objectos do tipo ConversaoRGBThread que
definem os fios de execução utilizados na operação de conversão
da imagem do espaço RGB para escala de cinzentos.
*/

import java.lang.Thread;
import java.awt.image.BufferedImage;

public class ConversaoRGBThread extends Thread {

    private BufferedImage image;     // Objecto que descreve a imagem através um buffer que permite aceder aos dados da mesma
    private int imageHeight;         // Altura de imagem
    private int[][] intensities;     // Array bidimensional que a armezena as intensidades de cada pixel da imagem
    private int[][][] LUT;           // Array tridimensional que representa uma tabela de consultas das codificações RGB
    private int startCol;            // Indice da primeira coluna de processomento
    private int endCol;              // Indice da utilma coluna de processmanto

    /*
     * Método Construtor
     * Cria um objecto do tipo ConversaoRGBThread.
     * @param image Objecto que descreve a imagem
     * @param imageHeight Altura da imagem
     * @param intensities Array bidimensional que a armezena as intensidades de cada pixel da imagem
     * @param LUT Array tridimensional que representa uma tabela de consultas das codificações RGB
     * @param startCol Indice da primeira coluna de processomento
     * @param endCol Indice da utilma coluna de processmanto
     */
    public ConversaoRGBThread(BufferedImage image,
                              int imageHeight,
                              int[][] intensities,
                              int[][][] LUT,
                              int startCol,
                              int endCol){
        this.image = image;
        this.imageHeight = imageHeight;
        this.intensities = intensities;
        this.LUT = LUT;
        this.startCol = startCol;
        this.endCol = endCol;
    }

    /*
     * Método de execução do fio.
     * Preenche uma parte pré-detereminada do array de intensidades,
     * com as intensidades armazenadas na tabela da consultas.
     */
    @Override
    public void run(){
        int rgb, r, g, b;

        for (int i = 0; i < this.imageHeight; i++) {
            for (int j = this.startCol; j < this.endCol; j++) {

                rgb = this.image.getRGB(j, i);

                r = (rgb & 0xff0000) >> 16;
                g = (rgb & 0xff00) >> 8;
                b = rgb & 0xff;

                this.intensities[i][j] = this.LUT[r][g][b];
            }
        }
    }
}
