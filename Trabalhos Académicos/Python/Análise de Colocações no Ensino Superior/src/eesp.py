# -*- coding: utf-8 -*-
"""
Nome do módulo: eesp.py
autores: Pedro Caixinha Nº4437 e Pedro Lopes Nº9850
data: 30/10/2013
Obs.: Este módulo inicia a aplicação.
"""
import wx
from gui import*

def main():
    """
    Função que corre o programa.
    """
    gui = wx.PySimpleApp(0)
    wx.InitAllImageHandlers()
    frame_1 = MyFrame(None, -1, "")
    gui.SetTopWindow(frame_1)
    frame_1.Show()
    gui.MainLoop()

if __name__ == "__main__": main()









