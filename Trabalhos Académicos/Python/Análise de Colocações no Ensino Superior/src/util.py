# -*- coding: utf-8 -*-
"""
Nome do módulo: util.py
autores: Pedro Caixinha Nº4437 e Pedro Lopes Nº9850
data: 30/10/2013
Obs.: Este módulo contém funções utilitárias de suporte à aplicação.
"""

import csv

def separa_dados(dados):

   """
   Função que devolve uma lista contendo duas partes de uma string inicial, sendo estas separadas pelo caracter '-'.
   """
   return dados.split(' - ')

def divide_nome(nome):

   """
   Função que devolve uma lista contendo duas partes de uma string inicial, sendo estas separadas pelo caracter ' '.
   """
   return nome.split(' ')

def criar_csv(nome_ficheiro, dados):

    """
    Função escreve num ficheiro csv os dados passados por parâmetro.

    Argumentos: 

        nome_ficheiro -- nome que será atribuído ao ficheiro gerado. 
    """
    
    with open(nome_ficheiro, 'wb') as csvfile:
        escritor = csv.writer(csvfile)

        for chave, valor in dados.items():
            if len(valor) == 3:
                escritor.writerow([chave, valor[0].encode('utf-8'), valor[1], valor[2]])     
            elif len(valor) == 2:
                escritor.writerow([chave, valor[0].encode('utf-8'), valor[1]])     
            else:
                pass    
    return 0    

if __name__ == "__main__": main()










